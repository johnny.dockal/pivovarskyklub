<?php
/**
 * 
 */
class Form_OrderStep2form extends Zend_Form {

    public function __construct($deliveryOptions, $deliveryCount = 1) {
        parent::__construct();
        //načteme view, ve kterém jsou již připravené texty v patřičném jazyce
        $view = Zend_Layout::getMvcInstance()->getView();
        if ($view->project == 'pivoklub') {
            $this->setMethod('POST')->setName('login')->setAction('/eshop/cart/order/?step=3');
        } else {
            $this->setMethod('POST')->setName('login')->setAction('/cart/order/?step=3z');
        }
        $this->setAttrib('class', 'orderform');         

        $delivery_options_formatted = array();
        foreach ($deliveryOptions as $value) { 
            $note = "";
            //ochcávka kvůli cenám v německu, teď se to musí převést do eura z korun
            $session   = new Zend_Session_Namespace('Default');
            if ($session->locale == 'de') {
                $price = $view->currency->exchangeToEuroNumber($value['price']);
            } else {
                $price = $value['price'];
            }
            if (($deliveryCount > 1) && ($price > 0)) {
                $sum = $deliveryCount * $price;
                $priceIsRight = $deliveryCount . "x " . $price . " = +" . $sum;
                $note = "<p class='order-description'>*Vaše objednávka bude rozdělena do $deliveryCount balíčků. Za tuto cenu za dopravu můžete nakoupit ještě další zboží.</p>"; 
            } else {
                $priceIsRight = "+".$price;
            }
            //if (($value['public'] == 1) && !empty($price)) {
            if ($value['public'] == 1) {                
                $delivery_options_formatted[$value['shipping_id']] = "&nbsp;" . $value['title'] . " <p class='order-description'>" . $value['text'] . "</p><p class='order-description'>$view->str_price0: <strong>" . $view->currency->toCurrency($priceIsRight) . " </strong></p>" . $note;
            }
        }
        $delivery = new Zend_Form_Element_Radio('shipping_id', array(
                    'multiOptions' => $delivery_options_formatted,
                    'escape' => false,
                    'required' => true
                ));
        $delivery->setLabel($view->str_order_select_shipping);        
        
        $message = new Zend_Form_Element_Textarea('order_message', array('style' => 'height: 100px; width: 98%'));
        $message->setAttrib('maxlength','1000');  
        $message->setLabel($view->str_order_message);     
        
        $submit = new Zend_Form_Element_Submit('submit', array('label' => $view->str_order_continue, 'class' => "button active cufon"));

        $this->addElements(array($delivery, $message, $submit));
    }

}
