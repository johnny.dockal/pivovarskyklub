<?php
/**
 * Plugin zjištuje požadavky na změnu jazyka webu v parametru.
 * Pokud odpovídá českému nebo anglickému jazyku, nastaví ji do Zend session.
 * @author Daniel Vála
 */
class Plugin_LangSelector extends Zend_Controller_Action_Helper_Abstract {

    public function init() {
        //nahodím seznam chybových hlášek, na konci zkontroluji, zda je prázdný, pokud neni, pošlu do view
        $errormessage = array();
        //zkontrolujeme, které jazyky jsou umožněny
        $config = Zend_Registry::get('config');
        //umožněné jazyky v konfigu jsou oddělené mezerou
        $langArray['enabled']   = explode(" ", $config->settings->languages->enabled);
        $langArray['localized'] = explode(" ", $config->settings->languages->localized);
        //zkontrolujeme, zda jsou kódy pro jazyky dobře nastavené a mají 2 charaktery
        foreach ($langArray['enabled'] as $value) {
            if (strlen($value) !== 2) {
                array_push($errormessage, "Chyba v inicializaci jazků v config.ini - parametr settings.languages.enabled, 
                                           kód neodpovídá správné délce znaků, nebo chybí dělení mezerou");
            }
        }
        
        $defaultSession = new Zend_Session_Namespace('Default');
        // pokud ještě není nastaven žádný jazyk, nastaví se defaultně prvně uvedený jazyk v configu
        if (!isset($defaultSession->lang)) {
            $defaultSession->lang = $langArray['enabled'][0];
        }

        // pokud je zjištěn požadavek na změnu jazyka, kontroluje se, zda je jazyk v požadavku jeden z uvedených v konfigu a nastaví se
        // pokud nevyhovuje nebo není vůbec nastaven, zachová se stávající jazyk
        $lang = $this->getRequest()->getParam('lang');
        if (isset($lang)) {
            if (in_array($lang, $langArray['enabled'])) {
                $defaultSession->lang = $lang;
            } else {
                array_push($errormessage, "Chyba v požadavku na změnu jazyka, jazyk nenalezen v config.ini");
            }
        }
        //nahodíme view, abychom do něj mohli posílat případné errormessage a stringtabley
        $view = Zend_Layout::getMvcInstance()->getView();
        if (isset($errormessage)) {
            $view->errormessage = $errormessage;
        }        

        //pošleme do view seznam jazyků, podle toho se budou nastavovat vlaječky
        $view->langArray = $langArray;
        
        //stringtable
        if ($defaultSession->lang == 'en') {
            $view->nacepu           = 'Currently on tap';
            $view->galerie          = 'Photogallery';
            $view->mapa             = 'MAP';
            $view->otviracka        = 'Mon - Sun';
            $view->klub_pravidla    = 'Členství klubu - pravidla';
            $view->klub_akce        = 'Klubové akce';
            $view->klub_jine_clanky = 'Jiné články';
            
            $view->form_name        = 'Name:';
            $view->form_surname     = 'Surname:';
            $view->form_phone       = 'Phone number:';
            $view->form_email       = 'E-mail:';
            $view->form_address     = 'Address:';
            $view->form_city        = 'City:';
            $view->form_zip         = 'Zip code:';
            $view->form_country     = 'Country:';
            $view->form_delivery    = 'Delivery:';
            $view->form_payment     = 'Payment:';
            //cena            
            $view->str_price0 = "Price: ";
            $view->str_price1 = "price without VAT: ";
            $view->str_price2 = "price including VAT: ";
            $view->str_price3 = "price (per item): ";
            $view->str_price4 = "Price per meter: ";
            $view->str_price5 = "Price per 0,5 meter: ";
            $view->str_price6 = "Price per m&sup2;: ";
            //objednávka
            $view->str_ask = "Ask about this product";
            $view->str_custom = "Order custom size";
            $view->str_available = "Pcs available:";
            $view->str_pile = "Pile: ";
            $view->str_pile1 = "cm";
            $view->str_grams = "Weight/m&#178;: : ";
            $view->str_grams1 = " g/m&#178;";
            $view->str_size = "Size: ";
            $view->str_material = "Material:";
            $view->str_production = "Production:";
            $view->str_size = "Size:";
            $view->str_weight = "Weight:";
            $view->str_stock = "In stock:";
            $view->str_pcs = "pcs";
            $view->str_add = "Add to basket";
            $view->str_empty = "This category is empty";
            $view->str_question = "Ask about this product";
            $view->str_care = "Care";
            $view->str_coarse1 = "Mountain Man (course) – one feather - the&nbsp;Rhodopa blankets";
            $view->str_coarse2 = "Rustic (slightly course) – two feathers - the&nbsp;Perelika blankets";
            $view->str_coarse3 = "Gentle (soft) – three feathers - the&nbsp;Karandilla blankets";
            $view->str_coarse4 = "Downy (ultra soft) – four feathers - the Merino&nbsp;blankets";
            $view->str_bysoftness = "By softness";
            $view->str_softness_text = "Softness";
            $view->str_softness_desc = "The most of the blankets in this category have the softness marked below";
            $view->str_softness_1 = "Mouintain man";
            $view->str_softness_2 = "Rustic";
            $view->str_softness_3 = "Gentle";
            $view->str_softness_4 = "Downy";
            //objednávání
            $view->str_order = "Order";
            $view->str_order_id = "Order ID:";
            $view->str_order_status = "Order status:";
            $view->str_order_timestamp = "Date of receipt:";
            $view->str_order_shipping_address = "Mailing address:";
            $view->str_order_step = "step";
            $view->str_order_continue = "Continue";
            $view->str_order_back = "Back";
            $view->str_order_empty_basket = "Empty basket";
            $view->str_order_fill_order = "Fill in form &#187;";
            $view->str_order_send = "Send form";
            $view->str_order_select = "Choose your pick-up location";
            $view->str_order_select_shipping = "Choose preferred shipping:";
            $view->str_order_select_payment = "Choose preferred payment:";
            $view->str_order_message = "Message for us (optional)";
            $view->str_order_product = "Product name";
            $view->str_order_size = "Size";            
            $view->str_order_quantity = "Quantity";
            $view->str_order_unit_price = "Price per piece";
            $view->str_order_total_price = "Total price";
            $view->str_order_total = "Total ";        
            $view->str_order_percent_of = "of";
            $view->str_order_details = "Order details ";
            $view->str_order_payment = "Payment details ";
            $view->str_order_VATincluded = "All prices include VAT.";
            $view->str_order_check = "You can check the status of your order here:";
            $view->str_order_link = "Show order status";
            $view->str_order_ready = "The order is ready for pickup";
            $view->str_order_sent = "Your order has been shipped";
            $view->str_order_subject = "We have recieved your order";
            $view->str_order_packing = "package"; 
            $view->str_order_paypal = "Pay via PayPal"; 
            $view->str_order_agree2 = "Souhlasím s obchodními podmínkami (viz. níže).";
            
            $view->menu_jidlo       = 'Menu';
            $view->menu_nabidka     = 'Lunch menu';
            $view->menu_obedy       = 'Speacial of the day';
            $view->menu_menu        = 'Complete menus';
            
            $view->menu_pivovice    = 'Beer spirits';
            $view->menu_destilaty   = 'Liquors';
            $view->menu_nealko      = 'Soft drinks';
            $view->menu_ostatni     = 'Other Beverages';
            
            $view->status_prijata   = 'order accepted';
            $view->status_vrizeni   = 'order is being processed';
            $view->status_odeslana  = 'order shipped';
            $view->status_vyrizena  = 'order payed for and finalised';
            $view->status_zrusena   = 'order cancelled';
            
            //chyby v objednávacím formuláři
            $view->err_fill_order = "Please fill in your method of payment.";
            $view->err_fill_address1 = "You must select a shipping address.";
            $view->err_fill_shipping = "Please fill out your method of shipping.";
            $view->err_fill_name = "Please fill in your name.";
            $view->err_fill_surname = "Please fill in your surname.";
            $view->err_fill_phone = "Please fill in your phone number.";
            $view->err_fill_email = "Please fill in a valid email address.";
            $view->err_fill_address2 = "Please fill in your address.";
            $view->err_fill_city = "Please fill in your city.";
            $view->err_fill_zip = "Please fill in your zip code (numbers only).";
            $view->err_fill_country = "Please fill in your country.";
            $view->err_fill_agree = "You must agree with our terms and conditions to finalise your order.";
                        
            $view->lang = 'en';
            
            $view->week = $array = array("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday");
            //$view->month = $array = array("", "January", "Febuary", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
            $view->month = $array = array("", "JANUARY", "FEBUARY", "MARCH", "APRIL", "MAY", "JUNE", "JULY", "AUGUST", "SEPTEMBER", "OCTOBER", "NOVEMBER", "DECEMBER");
            
        } else {
            $view->nacepu           = 'Právě na čepu';
            $view->galerie          = 'Fotogalerie';
            $view->mapa             = 'MAPA';
            $view->otviracka        = 'PO - NE';
            $view->otviracka_pracovni = 'PO - PÁ';
            $view->otviracka_vikend = 'SO - NE';
            $view->klub_pravidla    = 'Členství klubu - pravidla';
            $view->klub_akce        = 'Klubové akce';
            $view->klub_jine_clanky = 'Jiné články';         
            
            $view->form_name        = 'Jméno:';
            $view->form_surname     = 'Příjmení:';
            $view->form_com_name    = 'Jméno firmy:';
            $view->form_com_id      = 'IČO:';
            $view->form_phone       = 'Telefon:';
            $view->form_email       = 'E-mail:';
            $view->form_address     = 'Adresa:';
            $view->form_city        = 'Město:';
            $view->form_zip         = 'PSČ:';
            $view->form_address2    = 'Doručovací adresa (uveďte pouze pokud se liší od adresy firmy):';
            $view->form_city2       = 'Město:';
            $view->form_zip2        = 'PSČ:';
            $view->form_address3    = 'Fakturační adresa:';
            $view->form_city3       = 'Město:';
            $view->form_zip3        = 'PSČ:';
            $view->form_country     = 'Země:';
            $view->form_delivery    = 'Doprava:';
            $view->form_payment     = 'Platba:';
            
            $view->menu_jidlo       = 'Stálý jídelní lístek';
            $view->menu_nabidka     = 'Polední nabídka';
            $view->menu_obedy       = 'Nabídka dne';
            $view->menu_menu        = 'Nabídka menu';
            
            $view->menu_pivovice    = 'Pivní pálenky';
            $view->menu_destilaty   = 'Ovocné destiláty';
            $view->menu_nealko      = 'Nealko';
            $view->menu_ostatni     = 'Ostatní nápoje';
            
            $view->status_prijata   = 'objednávka přijata';
            $view->status_vrizeni   = 'objednávka se vyřizuje';
            $view->status_odeslana  = 'zboží je expedováno';
            $view->status_vyrizena  = 'objednávka vyřízena a zaplacena';
            $view->status_zrusena   = 'objednávka zrušena';
            
            //cena
            $view->str_price0 = "cena: ";
            $view->str_price1 = "cena bez DPH: ";
            $view->str_price2 = "cena včetně DPH: ";
            $view->str_price3 = "cena (za jednotku): ";
            $view->str_price4 = "cena (za bm): ";
            $view->str_price5 = "0,5 bm: ";
            $view->str_price6 = "cena za m&sup2;: ";
            //objednávka  
            $view->str_order = "Objednávka";
            $view->str_order_id = "Identifikační číslo:";
            $view->str_order_status = "Stav objednávky:";
            $view->str_order_timestamp = "Datum přijetí:";
            $view->str_order_shipping_address = "Doručovací adresa:";
            $view->str_order_step = "krok";
            $view->str_order_continue = "Pokračovat";
            $view->str_order_back = "Zpět";
            $view->str_order_empty_basket = "Vyprázdnit košík";
            $view->str_order_fill_order = "Vyplnit objednávku";
            $view->str_order_send = "Odeslat objednávku";
            $view->str_order_select = "Vyberte místo odběru";
            $view->str_order_select_shipping = "Vyberte způsob doručení:";
            $view->str_order_select_payment = "Vyberte způsob platby:";
            $view->str_order_message = "Zpráva pro příjemce:";
            $view->str_order_product = "Název výrobku";
            $view->str_order_size = "Velikost ";
            $view->str_order_quantity = "Množství";
            $view->str_order_unit_price = "Cena (ks) ";
            $view->str_order_unit_price_noVAT = "Cena (ks) <br/>(bez DPH)";
            $view->str_order_total_price = "Cena celkem ";
            $view->str_order_total_price_noVAT = "Cena celkem <br/>(bez DPH)";           
            $view->str_order_percent_of = "z částky";
            $view->str_order_total = "Celkem ";            
            $view->str_order_details = "Podrobnosti ";
            $view->str_order_payment = "Platba objednávky ";
            $view->str_order_VATincluded = "Všechny ceny jsou uvedené včetně DPH.";
            $view->str_order_check = "Stav objednávky můžete zkontrolovat zde:";
            $view->str_order_link = "Zobrazit objednávku";
            $view->str_order_ready = "Objednané zboží je připraveno k vyzvednutí";
            $view->str_order_sent = "Vaše objednávka byla předána přepravci";
            $view->str_order_subject = "Vaše objednávka byla uložena"; 
            $view->str_order_packing = "balíček"; 
            $view->str_order_paypal = "Zaplatit přes PayPal";
            $view->str_order_agree2 = "Souhlasím s obchodními podmínkami (viz. níže).";
            //chyby v objednávacím formuláři
            $view->err_fill_order = "Prosím vyplňte způsob platby za objednávku.";
            $view->err_fill_address1 = "Musíte vybrat doručovací adresu.";
            $view->err_fill_shipping = "Prosím vyplňte způsob doručení zboží.";
            $view->err_fill_name = "Prosím vyplňte své jméno.";
            $view->err_fill_surname = "Prosím vyplňte své přijmení.";
            $view->err_fill_phone = "Prosím vyplňte své telefonní číslo.";
            $view->err_fill_email = "Prosím vyplňte validní email adresu.";
            $view->err_fill_address2 = "Prosím vyplňte svou adresu.";
            $view->err_fill_city = "Prosím vyplňte město.";
            $view->err_fill_zip = "Prosím vyplňte PSČ (pouze číselné hodnoty).";
            $view->err_fill_country = "Prosím vyplňte Zemi doručení.";
            $view->err_fill_agree = "Musíte souhlasit s obchodními podmínkami.";
            
            $view->lang = 'cz';
                       
            $view->week = $array = array("neděle", "pondělí", "úterý", "středa", "čtvrtek", "pátek", "sobota");
            //$view->month = $array = array ("", "leden", "únor", "březen", "duben", "květen", "červen", "červenec", "srpen", "září", "říjen", "listopad", "prosinec");
            $view->month = $array = array ("", "LEDEN", "ÚNOR", "BŘEZEN", "DUBEN", "KVĚTEN", "ČERVEN", "ČERVENEV", "SRPEN", "ZÁŘÍ", "ŘÍJEN", "LISTOPAD", "PROSINEC");
        } 
        //načteme si všechny důležité texty na všechny stránky z databáze
        $modelTexts = new Model_DbTable_Texts();
        $texts = $modelTexts->fetchTexts();
        $view->texts = $texts;
    }

}
