<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class Model_DbTable_OrderHistory extends Zend_Db_Table_Abstract {

    protected $_name = 'eshop_order_history';
    protected $_primary = 'order_id';
    
    public function fetchOrderHistory($order_id) {
        $db = Zend_Db_Table::getDefaultAdapter();
        $query = "SELECT "
                . "oh.order_history_id, "
                . "oh.order_id, "
                . "u.user_name"
                . "oh.user_id, "
                . "oh.status, "
                . "oh.timestamp, "
                . "oh.note "
                . "FROM $this->_name AS oh "
                . "LEFT JOINusers AS u ON oh.user_id = u.user_id "
                . "WHERE order_id = '$order_id'"
                . "ORDER BY oh.timestamp";
        return $db->fetchAll($query);
    }
    
    public function saveOrderHistory($data) {
        $order_id = null;
        try {
            $this->insert($data);
            $db = Zend_Db_Table::getDefaultAdapter();
            $order_id = $db->lastInsertId($this->_name, $this->_primary);
        } catch (Zend_Exception $e) {
            echo "Caught exception: " . get_class($e) . "\n";
            echo "Message: " . $e->getMessage() . "\n";
        }        
    
        return $order_id;  
    }
}