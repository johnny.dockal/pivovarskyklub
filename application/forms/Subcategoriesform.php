<?php

/**
 * Formulář k přihlášení se do administrace.
 *
 * @package default
 * @author Daniel Vála
 */
class Form_Subcategoriesform extends Zend_Form {

    public function __construct($subcategory, $action = '/admin/subcategories/save/') {
        parent::__construct($subcategory);
        $this->setMethod('POST')->setName('login')->setAction($action);
        $this->setAttrib('class', 'admintable');

        $subcategory_id = new Zend_Form_Element_Hidden('subcategory_id');
        $subcategory_id->setDecorators(array('ViewHelper'));
        $subcategory_id->setValue($subcategory['subcategory_id']);
        $this->addElement($subcategory_id);

        $sequence = new Zend_Dojo_Form_Element_TextBox('sequence');
        $sequence->setLabel('Sekvence:')->setRequired(true);
        $sequence->setValue($subcategory['sequence']);
        $this->addElement($sequence);

        $public = new Zend_Form_Element_Radio('public');
        $public->setLabel('Veřejné?')->setRequired(true);
        $public->addMultiOption(1, 'ano');
        $public->addMultiOption(0, 'ne');
        $public->setValue($subcategory['public']);
        $this->addElement($public);

        $category_id = new Zend_Form_Element_Select('category_id', array('class' => 'textboxwide'));
        $category_id->setLabel('Kategorie:');
        $model = new Model_DbTable_EshopCategories();
        $options = $model->fetchAll()->toArray();
        foreach ($options as $value) {
            $category_id->addMultiOption($value['category_id'], $value['title_cz']);
        }
        $category_id->setValue($subcategory['category_id']);
        $this->addElement($category_id);

        if (isset($subcategory['title_cz'])) {
            $title_cz = new Zend_Dojo_Form_Element_TextBox('title_cz', array('class' => 'textboxwide'));
            $title_cz->setLabel('Nadpis česky:')->setRequired(true);
            $title_cz->setValue($subcategory['title_cz']);
            $this->addElement($title_cz);
        }
        if (isset($subcategory['text_cz'])) {
            $text_cz = new Zend_Dojo_Form_Element_Textarea('text_cz', array('class' => "textboxhuge"));
            $text_cz->setLabel('Text česky:')->setRequired(true);
            $text_cz->setValue($subcategory['text_cz']);
            $this->addElement($text_cz);
        }
        if (isset($subcategory['title_en'])) {
            $title_en = new Zend_Dojo_Form_Element_TextBox('title_en', array('class' => 'textboxwide'));
            $title_en->setLabel('Nadpis anglicky:')->setRequired(true);
            $title_en->setValue($subcategory['title_en']);
            $this->addElement($title_en);
        }
        if (isset($subcategory['text_en'])) {
            $text_en = new Zend_Dojo_Form_Element_Textarea('text_en', array('class' => "textboxhuge"));
            $text_en->setLabel('Text anglicky:')->setRequired(true);
            $text_en->setValue($subcategory['text_en']);
            $this->addElement($text_en);
        }
        if (isset($subcategory['title_de'])) {
            $title_de = new Zend_Dojo_Form_Element_TextBox('title_de', array('class' => 'textboxwide'));
            $title_de->setLabel('Nadpis německy:')->setRequired(true);
            $title_de->setValue($subcategory['title_de']);
            $this->addElement($title_de);
        }
        if (isset($subcategory['text_de'])) {
            $subcategory_de = new Zend_Dojo_Form_Element_Textarea('text_de', array('class' => "textboxhuge"));
            $subcategory_de->setLabel('Text německy:')->setRequired(true);
            $subcategory_de->setValue($subcategory['text_de']);
            $this->addElement($subcategory_de);
        }
        if (isset($subcategory['title_ru'])) {
            $title_ru = new Zend_Dojo_Form_Element_TextBox('title_ru', array('class' => 'textboxwide'));
            $title_ru->setLabel('Nadpis rusky:')->setRequired(true);
            $title_ru->setValue($subcategory['title_ru']);
            $this->addElement($title_ru);
        }
        if (isset($subcategory['text_ru'])) {
            $subcategory_ru = new Zend_Dojo_Form_Element_Textarea('text_ru', array('class' => "textboxhuge"));
            $subcategory_ru->setLabel('Text rusky:')->setRequired(true);
            $subcategory_ru->setValue($subcategory['text_ru']);
            $this->addElement($subcategory_ru);
        }

        $submit = new Zend_Form_Element_Submit('submit', array('label' => "Uložit", 'class' => "savebutton"));
        $this->addElement($submit);
    }

}
