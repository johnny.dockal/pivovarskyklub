<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class Model_DbTable_EshopPayments extends Zend_Db_Table_Abstract {

    protected $_name = 'eshop_payments';
    protected $_primary = 'payment_id';
    
    public function fetchPayments($shipping_id = null, $shipping_count = null) {
        $session = new Zend_Session_Namespace('Default');
        $query = $this->select()->from($this->_name, array('payment_id', 'public', 'title_'.$session->lang.' AS title', 'text_'.$session->lang.' AS text', 'price_'.$session->locale.' AS price', 'price_percent AS percentage'));
        $result = $this->fetchAll($query)->toArray();
        //ochcávka kvůli tomu, aby se nezobrazovala dobírka u osobního odběru
        if ($shipping_id == 1) {
            unset($result[3]);
        }
        return $result;
    }
}