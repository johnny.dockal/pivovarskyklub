<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Model_DbTable_Products extends Zend_Db_Table_Abstract {

    protected $_name = 'eshop_products';
    protected $_primary = 'product_id';
    protected $_limit = 30;
    protected $_productQuery_grouped = 
                        "SELECT     ep.product_id, 
                                    ep.status,
                                    GROUP_CONCAT(es.category_id SEPARATOR ';') AS category_id,
                                    GROUP_CONCAT(es.subcategory_id SEPARATOR ';') AS subcategory_id,
                                    GROUP_CONCAT(es.title_cz SEPARATOR ';') AS subcategory_title,
                                    es.type AS type,
                                    ep.title_cz AS title,
                                    ep.text_cz AS text,
                                    ep.producer,
                                    ep.size,
                                    ep.price,
                                    ec.category_id,
                                    ec.title_cz AS category_title,
                                    ec.text_cz AS category_text,
                                    ep.vat_rate AS vat_rate,
                                    ev.vat AS vat
                        FROM eshop_products AS ep JOIN (eshop_subcat_products AS esp, eshop_subcategories AS es, eshop_categories AS ec) 
                            ON (esp.product_id = ep.product_id AND es.subcategory_id = esp.subcategory_id AND ec.category_id = es.category_id) 
                                JOIN eshop_vat AS ev ON ev.vat_rate = ep.vat_rate";
    protected $_productQuery = 
                        "SELECT     ep.product_id, 
                                    ep.status,
                                    es.subcategory_id,
                                    es.title_cz AS subcategory_title,
                                    es.type,
                                    ep.title_cz AS title,
                                    ep.text_cz AS text,
                                    ep.producer,
                                    ep.size,
                                    ep.price,
                                    ec.category_id,
                                    ec.title_cz AS category_title,
                                    ec.text_cz AS category_text,
                                    ep.vat_rate AS vat_rate,
                                    ev.vat AS vat
                        FROM eshop_products AS ep LEFT JOIN (eshop_subcat_products AS esp, eshop_subcategories AS es, eshop_categories AS ec) 
                            ON (esp.product_id = ep.product_id AND es.subcategory_id = esp.subcategory_id AND ec.category_id = es.category_id) 
                                JOIN eshop_vat AS ev ON ev.vat_rate = ep.vat_rate";
    protected $_productQueryEdit = 
                        "SELECT     ep.product_id, 
                                    ep.status,
                                    es.subcategory_id,
                                    es.type,
                                    es.title_cz AS subcategory_title,
                                    ep.title_cz,
                                    ep.title_en,
                                    ep.text_cz,
                                    ep.text_en,
                                    ep.producer,
                                    ep.size,
                                    ep.price,
                                    ec.category_id,
                                    ec.title_cz AS category_title,
                                    ec.text_cz AS category_text,
                                    ep.vat_rate AS vat_rate,
                                    ev.vat AS vat
                        FROM eshop_products AS ep LEFT JOIN (eshop_subcat_products AS esp, eshop_subcategories AS es, eshop_categories AS ec) 
                            ON (esp.product_id = ep.product_id AND es.subcategory_id = esp.subcategory_id AND ec.category_id = es.category_id) 
                                JOIN eshop_vat AS ev ON ev.vat_rate = ep.vat_rate";
    public function fetchProduct($product_id) {
        $db = Zend_Db_Table::getDefaultAdapter();
        try {
            $query = $this->_productQuery_grouped . "WHERE ep.product_id = $product_id GROUP BY ep.product_id ";
            $product = $db->fetchRow($query);
        } catch (Zend_Exception $e) {
            echo "Caught exception: " . get_class($e) . "\n";
            echo "Message: " . $e->getMessage() . "\n";
        }
        //Pokud napoprvé není žádný výsledek, může se jednat o produkt, který není zařazen do žádné subkategorie
        if (empty($product)) {
            try {
                $query = $this->_productQuery . "WHERE ep.product_id = $product_id";
                $product = $db->fetchRow($query);
            } catch (Zend_Exception $e) {
                echo "Caught exception: " . get_class($e) . "\n";
                echo "Message: " . $e->getMessage() . "\n";
            }
        }

        return $product;
    }
    
    public function fetchProductEdit($product_id) {
        $db = Zend_Db_Table::getDefaultAdapter();
        try {
            $query = $this->_productQueryEdit . "WHERE ep.product_id = $product_id GROUP BY ep.product_id ";
            $product = $db->fetchRow($query);
        } catch (Zend_Exception $e) {
            echo "Caught exception: " . get_class($e) . "\n";
            echo "Message: " . $e->getMessage() . "\n";
        }
 
        return $product;
    }
    
    public function fetchProductsByIds($idArray) {  
        $query = $this->_productQuery . "WHERE ep.product_id IN (";
        $i = 0;
        foreach ($idArray as $value) {
            if ($i > 0) {$query .= ", ";}
            $query .= "$value";
            $i++;
        }
        $query .= ")";
        try {
            $db = Zend_Db_Table::getDefaultAdapter();
            $result = $db->fetchAll($query);
        } catch (Zend_Exception $e) {
            echo "<br/>Caught exception: " . get_class($e) . "\n";
            echo "<br/>Message: " . $e->getMessage() . "\n";
            echo "<br/>SQL: " . $query;
        }
        return $result;
    }

    public function fetchProductsByCat($category_id = null, $order = null) {
        //$defaultSession = new Zend_Session_Namespace('Default');        
        //připravím
        if (!empty($category_id)) {
            if ($category_id == 'unsorted') {
                $query = $query = $this->_productQuery . "WHERE ec.category_id IS NULL ";
            } else if ($category_id == 'all') {
                $query = $this->_productQuery_grouped . "GROUP BY ep.product_id ";
            } else {
                $query = $this->_productQuery_grouped . "WHERE ec.category_id = $category_id GROUP BY ep.product_id ";
            }
        } else {
            $query = $this->_productQuery_grouped . "GROUP BY ep.product_id ";
        }
        //zjistím podle čeho to seřadit
        if (!empty($order)) {
            $query .= "ORDER BY $order";
        }
        //pošlu query
        try {
            $db = Zend_Db_Table::getDefaultAdapter();
            $result = $db->fetchAll($query);
        } catch (Zend_Exception $e) {
            echo "<br/>Caught exception: " . get_class($e) . "\n";
            echo "<br/>Message: " . $e->getMessage() . "\n";
            echo "<br/>SQL: " . $query;
        }
        return $result;
    }

    public function fetchProductsBySubcat($subcategory_id = null) {
        $defaultSession = new Zend_Session_Namespace('Default');
        $defaultSession->lang;
        if (!empty($subcategory_id)) {
            $db = Zend_Db_Table::getDefaultAdapter();
            $query = $this->_productQuery . "WHERE esp.subcategory_id = $subcategory_id ";
            //$query = "SELECT * FROM eshop_categories AS ec LEFT JOIN (eshop_subcategories AS es, eshop_subcat_products AS esp, eshop_products AS ep) ON (ec.category_id = es.category_id AND es.subcategory_id = esp.subcategory_id AND esp.product_id = ep.product_id) WHERE es.subcategory_id = $subcategory_id";
            $result = $db->fetchAll($query);
        }
        return $result;
    }
    
    public function fetchProductsByGroup($group_id = null) {
        $defaultSession = new Zend_Session_Namespace('Default');
        $defaultSession->lang;
        if (!empty($group_id)) {
            $db = Zend_Db_Table::getDefaultAdapter();
            $query = "SELECT     
                        eg.group_id, 
                        eg.title_cz AS group_title,
                        esg.subcategory_id,
                        ep.product_id, 
                        ep.status,
                        es.subcategory_id,
                        es.title_cz AS subcategory_title,
                        es.type,
                        ep.title_cz AS title,
                        ep.text_cz AS text,
                        ep.producer,
                        ep.size,
                        ep.price
                        FROM eshop_groups AS eg
                        JOIN (eshop_subcat_groups AS esg, eshop_subcategories AS es, eshop_products AS ep, eshop_subcat_products AS esp) 
                            ON (eg.group_id = esg.group_id AND esg.subcategory_id = es.subcategory_id AND esp.product_id = ep.product_id AND es.subcategory_id = esp.subcategory_id) 
                        WHERE eg.group_id = '$group_id'";
            $result = $db->fetchAll($query);
        }
        return $result;
    }

    public function saveProduct($data, $subcats, $product_id = null) {
        $relationmodel = new Model_DbTable_SubcatProducts();
        $subcatmodel = new Model_DbTable_SubCategories();
        if (isset($product_id)) {
            $where = $this->getAdapter()->quoteInto('product_id = ?', $product_id);
            $this->update($data, $where);
            //vezmeme celý seznam subkategorií
            $subcategories = $subcatmodel->fetchAll()->toArray();
            //zjistíme, které subkategorie byli již k výrobku přiřazeny
            $prevselected = $relationmodel->fetchProductSubcats($product_id);
            foreach ($subcategories as $value) {
                if ((!in_array($value['subcategory_id'], $prevselected)) && (in_array($value['subcategory_id'], $subcats))) {
                    //subkategorie z postu není na seznamu z databáze, musí se tam přidat
                    $relationmodel->insert(array('subcategory_id' => $value['subcategory_id'], 'product_id' => $product_id));
                } else if ((in_array($value['subcategory_id'], $prevselected)) && (!in_array($value['subcategory_id'], $subcats))) {
                    $where = array();
                    $where[] = $relationmodel->getAdapter()->quoteInto('subcategory_id = ?', $value['subcategory_id']);
                    $where[] = $relationmodel->getAdapter()->quoteInto('product_id = ?', $product_id);
                    $relationmodel->delete($where);
                }
            }
        } else {
            $this->insert($data);
            $db = Zend_Db_Table::getDefaultAdapter();
            $product_id = $db->lastInsertId($this->_name, $this->_primary);
            foreach ($subcats as $value) {
                $relationmodel->insert(array('subcategory_id' => $value, 'product_id' => $product_id));
            }
        }
        return $product_id;
    }

}
