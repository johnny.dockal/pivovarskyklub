<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class Model_DbTable_Deliveries extends Zend_Db_Table_Abstract {

    protected $_name = 'eshop_deliveries';
    protected $_primary = 'delivery_id';
    
    public function fetchDeliveries() {
        $defaultSession = new Zend_Session_Namespace('Default');
        $query = $this->select()->from($this->_name, array('delivery_id', 'public', 'title_'.$defaultSession->lang.' AS title', 'text_'.$defaultSession->lang.' AS text', 'price'));
        $result = $this->fetchAll($query)->toArray();
        return $result;
    }
}