<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Model_DbTable_EshopOrders extends Zend_Db_Table_Abstract {

    protected $_name = 'eshop_orders';
    protected $_primary = 'order_id';

    public function fetchOrders($year = null, $filter = null) {
        $db = Zend_Db_Table::getDefaultAdapter();
        $sql = "SELECT "
                . "o.order_id, "
                . "o.status_id, "
                . "o.payment_id, "
                . "o.shipping_id, "
                . "o.payment_price, "
                . "o.shipping_price, "
                . "o.user_id, "
                . "o.order_timestamp, "
                . "o.order_lang, "
                . "o.order_name, "
                . "o.order_surname, "
                . "o.order_phone, "
                . "o.order_email, "
                . "o.order_sendmail, "
                . "o.order_address, "
                . "o.order_city, "
                . "o.order_zip, "
                . "u.user_name, "
                . "s.title_cz AS title, "
                . "s.text_cz AS text, "
                . "s.color, "
                . "SUM(d.product_price * d.product_quantity) + (o.payment_count * o.payment_price) + (o.shipping_count * o.shipping_price) + (IFNULL(o.packing_count, 0) * IFNULL(o.packing_price, 0)) AS order_price, "
                . "SUM(d.product_quantity) AS order_quantity "
                . "FROM $this->_name AS o "
                . "JOIN eshop_order_details AS d ON o.order_id = d.order_id "
                . "JOIN eshop_order_status AS s ON o.status_id = s.status_id "
                . "LEFT JOIN users AS u ON o.user_id = u.user_id ";
        if (isset($filter) && isset($year)) {
            $i = 0;
            $statuses = '';
            foreach ($filter as $value) {
                if (isset($value)) {
                    if ($i > 0) {
                        $statuses .= ", ";
                    }
                    $statuses .= "'$value'";
                    $i++;
                }
            }
            if ($year > 1) {
                $sql .= "WHERE YEAR(o.order_timestamp) = $year AND o.status_id IN ($statuses) ";
            } else {
                $sql .= "WHERE o.status_id IN ($statuses) ";
            }
        }
        $sql .= " GROUP BY o.order_id ORDER BY o.order_timestamp DESC";
        try {
            $result = $db->fetchAll($sql);
        } catch (Zend_Exception $e) {
            echo "<p>Caught exception in Orders->fetchOrders(): " . get_class($e) . "</p>";
            echo "<p>Message: " . $e->getMessage() . "</p>";
            echo "<p>SQL: $sql</p>";
        }
        foreach ($result as $value) {
            $value['order_price'] = $value['order_price'] + $value['payment_price'] + $value['shipping_price'];
        }
        return $result;
    }
    
    public function fetchOrdersByProduct($product_id, $date_from, $date_to) {
        $db = Zend_Db_Table::getDefaultAdapter();
        $sql = "SELECT "
                . "o.order_id, "
                . "o.status_id, "
                . "o.payment_id, "
                . "o.shipping_id, "
                . "o.payment_price, "
                . "o.shipping_price, "
                . "o.user_id, "
                . "o.order_timestamp, "
                . "o.order_lang, "
                . "o.order_name, "
                . "o.order_surname, "
                . "o.order_phone, "
                . "o.order_email, "
                . "o.order_sendmail, "
                . "o.order_address, "
                . "o.order_city, "
                . "o.order_zip, "
                . "u.user_name, "
                . "s.title_cz AS title, "
                . "s.text_cz AS text, "
                . "s.color, "
                . "SUM(d.product_price * d.product_quantity) + (o.payment_count * o.payment_price) + (o.shipping_count * o.shipping_price) AS order_price, "
                . "SUM(d.product_quantity) AS order_quantity "
                . "FROM $this->_name AS o "
                . "JOIN eshop_order_details AS d ON o.order_id = d.order_id "
                . "JOIN eshop_order_status AS s ON o.status_id = s.status_id "
                . "LEFT JOIN users AS u ON o.user_id = u.user_id "
                . "WHERE d.product_id = $product_id "
                . "AND o.order_timestamp >= '$date_from 00:00:00' AND o.order_timestamp <= '$date_to 23:59:59'";        
        $sql .= " GROUP BY o.order_id ORDER BY o.order_timestamp DESC";
        try {
            $result = $db->fetchAll($sql);
        } catch (Zend_Exception $e) {
            echo "<p>Caught exception in Orders->fetchOrders(): " . get_class($e) . "</p>";
            echo "<p>Message: " . $e->getMessage() . "</p>";
            echo "<p>SQL: $sql</p>";
        }
        foreach ($result as $value) {
            $value['order_price'] = $value['order_price'] + $value['payment_price'] + $value['shipping_price'];
        }
        return $result;
    }

    public function changeStatus($order_id, $status_id) {
        $user = Zend_Auth::getInstance()->getIdentity();
        $data = array('status_id' => $status_id, 'user_id' => $user->user_id);
        $where = $this->getAdapter()->quoteInto('order_id = ?', $order_id);
        try {
            if ($status_id >= 0) {
                $this->update($data, $where);
            }
            $model = new Model_DbTable_EshopOrderHistory();
            $data2 = array('order_id' => $order_id, 'status_id' => $status_id, 'user_id' => $user->user_id);
            $model->insert($data2);
        } catch (Zend_Exception $e) {
            echo "Caught exception: " . get_class($e) . "\n";
            echo "Message: " . $e->getMessage() . "\n";
        }
    }

    public function saveOrder($data) {
        $order_id = null;
        try {
            $db = Zend_Db_Table::getDefaultAdapter();
            $this->insert($data);
            $order_id = $db->lastInsertId($this->_name, $this->_primary);
            echo "<p>order id: $order_id</p>";
        } catch (Zend_Exception $e) {
            echo "Caught exception: " . get_class($e) . "\n";
            echo "Message: " . $e->getMessage() . "\n";
        }

        return $order_id;
    }

    public function getOrderByToken($token) {
        $sql = "SELECT * FROM $this->_name WHERE token = '$token'";
        try {
            $db = Zend_Db_Table::getDefaultAdapter();
            $result = $db->fetchAll($sql);
        } catch (Zend_Exception $e) {
            echo "Caught exception during saving order details: " . get_class($e) . "\n";
            echo "Message: " . $e->getMessage() . "\n";
        }
        return $result;
    }

}
