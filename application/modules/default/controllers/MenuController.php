<?php

class Default_MenuController extends Zend_Controller_Action
{
    private $action;

    public function init() {
        $this->action = $this->_getParam('view');  
        if (empty($this->action)) {
            $this->action = 'listek';
        }
        $this->view->action = $this->action;
    }

    public function indexAction() {
        $date = $this->_getParam('date');
        if (empty($date)) {
            $date = date('Y-m-d');
        }
        switch ($this->action) {
            //polední meníčka na celý týden
            case 'obedy':
                $table  = new Model_DbTable_Menus();
                $menudata = $table->fetchMenus($date, 1);
                $this->view->date = $date;
                $this->view->menudata = $menudata;  
                break;
            //menu pro skupiny
            case 'menu':
                $texts  = new Model_DbTable_Texts();
                $this->view->menu = $texts->fetchText('menu');
                break;
            //polední nabídka
            case 'nabidka':
                $texts  = new Model_DbTable_Texts();
                $this->view->menu = $texts->fetchText('nabidka', 'cz');
                break;
            //stálý jídelní lístek
            default:
                $texts  = new Model_DbTable_Texts();
                $this->view->menu = $texts->fetchText('jidelak', 'cz');
                break;
            
        }
        
    }
}

