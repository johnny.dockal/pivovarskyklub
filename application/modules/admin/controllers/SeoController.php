<?php

class Admin_SeoController extends Zend_Controller_Action {

    private $category_id = 'all';
    private $page = 1;
    private $order = 'product_id';
    private $sort = 'desc';
    private $limit = 50;

    public function init() {
        /* Initialize action controller here */
    }

    public function preDispatch() {
        $category_id = $this->getParam('category_id');
        $page = $this->getParam('page');
        $order = $this->getParam('order');
        $sort = $this->getParam('sort');

        $adminSession = new Zend_Session_Namespace('Admin');

        if (!empty($category_id)) {
            $this->category_id = $category_id;
            $adminSession->category_id = $category_id;
            //pokus se mění kategorie, musí se jít zpátky na stránku 1, aby nedošlo k out of bounds exception
            $adminSession->page = $this->page;
        } else {
            $this->category_id = $adminSession->category_id;
        }
        if (empty($adminSession->category_id)) {
            $adminSession->category_id = $this->category_id;
        }
        //nastavíme stránku
        if (!empty($page)) {
            $this->page = $page;
            $adminSession->page = $page;
        } else if (empty($adminSession->page)) {
            $adminSession->page = $this->page;
        } else {
            $this->page = $adminSession->page;
        }
        //nastavíme pořadí
        if (!empty($order)) {
            $this->order = $order;
            $adminSession->order = $order;
        } else {
            $this->order = $adminSession->order;
        }
        if (empty($adminSession->order)) {
            $adminSession->order = $this->order;
        }
        if (!empty($sort)) {
            $this->sort = $sort;
            $adminSession->sort = $sort;
        } else {
            $this->sort = $adminSession->sort;
        }
        if (empty($adminSession->sort)) {
            $adminSession->sort = $this->sort;
        }

        $this->view->category_id = $adminSession->category_id;
        $this->view->page = $adminSession->page;
        $this->view->order = $adminSession->order;
        $this->view->sort = $adminSession->sort;
    }

    public function indexAction() {
        $categories = new Model_DbTable_EshopCategories();
        $model = new Model_DbTable_EshopProducts();

        $products = $model->fetchProductsByCat($this->category_id, $this->order, false);
        $this->view->categories = $categories->fetchAll()->toArray();
        $this->view->itemlimit = $this->limit;
        $this->view->itemcount = count($products);
        $this->view->pagecount = ceil($this->view->itemcount / $this->limit);
        $this->view->products = array_chunk($products, $this->limit);
    }

    public function saveallAction() {
        $model = new Model_DbTable_EshopProducts();        
        $seo = new Model_SeoConverter();
        $products = $model->fetchAll("alias_cz = ''");
        foreach ($products as $product) {
            $data = array(
                'alias_cz' => $seo->makeAlias($product['title_cz'], 'cz', $product['product_id']),
                'alias_en' => $seo->makeAlias($product['title_en'], 'en', $product['product_id'])
            );
            $model->update($data, "product_id = '".$product['product_id']."'");
        }        
        $this->_redirect('/admin/seo/');
    }

}
