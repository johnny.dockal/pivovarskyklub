<?php

/**
 * Model tabulky article.
 *
 * @package default
 * @author Daniel Vála
 */
class Model_DbTable_Menus extends Zend_Db_Table_Abstract {

    protected $_name = 'menus';
    protected $_primary = 'menu_id';

    /* Tato funkce si zjistí, co je předaný den za den v týdnu a podle toho vybere 
     * z databáze pouze ty data, které jsou relevantní pro daný týden.
     */
    public function fetchMenus($date, $trynextweek = 0) {
        $year   = substr($date, 0, 4);
        $day    = substr($date, -2, 2);
        $month  = substr($date, 5, 2);
        
        //co je to za den
        $w = date('w');
        //neděle je standartně nula, to se mi nelíbí
        if ($w == 0) {
            $w = 7;
        }
        //pokud je sobota nebo neděle, tak se mrknem, jestli už je připravena nabídka na příští týden
        if (($w >= 6) && $trynextweek) {
            $x = (6 - $w);
            $date_start = date('Y-m-d', mktime(0,0,0,$month,$day+7-$w,$year));
            $date_end   = date('Y-m-d', mktime(0,0,0,$month,$day+7+$x,$year));
            $select     = $this->select()->where("menu_id >= '$date_start'")->where("menu_id <= '$date_end'");
            $result     = $this->fetchAll($select);
        }
        //pokud na příští týden nabídka ještě není (prázdný výsledek) tak tam dáme alespoň z toho týdne...
        if (empty($result) or (count($result) == 0)) {
            $w--;
            $x = (6 - $w);
            $date_start = date('Y-m-d', mktime(0,0,0,$month,$day-$w,$year));
            $date_end   = date('Y-m-d', mktime(0,0,0,$month,$day+$x+7,$year));
            $select     = $this->select()->where("menu_id >= '$date_start'")->where("menu_id <= '$date_end'");
            $result     = $this->fetchAll($select);
        }     
        $menuArray = array();
        foreach ($result as $value) {
            $menuArray[$value['menu_id']] = $value['menu_cz'];
        }
        return $menuArray;
    }
}