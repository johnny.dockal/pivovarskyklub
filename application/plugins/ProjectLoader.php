<?php
/**
 * Plugin načte layout pro aktuální modul.
 * @author Daniel Vála
 */
class Plugin_ProjectLoader extends Zend_Controller_Action_Helper_Abstract {

    public function init() {        
        $view = Zend_Layout::getMvcInstance()->getView();
        $view->project   = 'pivoklub';
    }
}
