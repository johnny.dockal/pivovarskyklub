<?php

/**
 * Vytvoření uživatelských rolí guests a admin.
 * Nastavení jejich oprávnění, tj. na jaké stránky mají přístup.
 * @author Daniel Vála
 */
class Model_UserAcl extends Zend_Acl {

    function __construct() {
        // Roles
        $this->addRole(new Zend_Acl_Role('guest'));
        $this->addRole(new Zend_Acl_Role('editor'), 'guest');
        $this->addRole(new Zend_Acl_Role('admin'), 'editor');
        $this->addRole(new Zend_Acl_Role('superadmin'), 'admin');
        
        // Resources
        // Default module
        $this->add(new Zend_Acl_Resource('default'))                
                ->add(new Zend_Acl_Resource('default:index', 'default'))
                ->add(new Zend_Acl_Resource('default:jidlo', 'default'))
                ->add(new Zend_Acl_Resource('default:eshopmaintenance', 'default'))
                ->add(new Zend_Acl_Resource('default:kluboveinfo', 'default'))
                ->add(new Zend_Acl_Resource('default:kontakt', 'default'))
                ->add(new Zend_Acl_Resource('default:menu', 'default'))
                ->add(new Zend_Acl_Resource('default:napoje', 'default'))
                ->add(new Zend_Acl_Resource('default:obedy', 'default'))
                ->add(new Zend_Acl_Resource('default:pivo', 'default'))
                ->add(new Zend_Acl_Resource('default:gallery', 'default'))
                ->add(new Zend_Acl_Resource('default:mailinglist', 'default'))
                ->add(new Zend_Acl_Resource('default:eshop', 'default'))
                ->add(new Zend_Acl_Resource('default:error', 'default'));  
        // Editor module
        $this->add(new Zend_Acl_Resource('editor'))
                ->add(new Zend_Acl_Resource('admin:authentication', 'admin'))
                ->add(new Zend_Acl_Resource('admin:nacepu', 'admin'));
        
        // Admin module
        $this->add(new Zend_Acl_Resource('admin'))
                ->add(new Zend_Acl_Resource('admin:gallery', 'admin'))
                ->add(new Zend_Acl_Resource('admin:articles', 'admin'))
                ->add(new Zend_Acl_Resource('admin:index', 'admin'))
                ->add(new Zend_Acl_Resource('admin:jidlo', 'admin'))
                ->add(new Zend_Acl_Resource('admin:nealko', 'admin'))
                ->add(new Zend_Acl_Resource('admin:kluboveinfo', 'admin'))
                ->add(new Zend_Acl_Resource('admin:kontakt', 'admin'))
                ->add(new Zend_Acl_Resource('admin:menu', 'admin'))
                ->add(new Zend_Acl_Resource('admin:nabidka', 'admin'))
                ->add(new Zend_Acl_Resource('admin:napoje', 'admin'))
                ->add(new Zend_Acl_Resource('admin:novinka', 'admin'))
                ->add(new Zend_Acl_Resource('admin:obedy', 'admin'))
                ->add(new Zend_Acl_Resource('admin:pivo', 'admin'))
                ->add(new Zend_Acl_Resource('admin:mail', 'admin'))
                ->add(new Zend_Acl_Resource('admin:error', 'admin'))
                ->add(new Zend_Acl_Resource('admin:mailinglist', 'admin'))
                
                ->add(new Zend_Acl_Resource('admin:categories', 'admin'))
                ->add(new Zend_Acl_Resource('admin:subcategories', 'admin'))
                ->add(new Zend_Acl_Resource('admin:orders', 'admin'))
                ->add(new Zend_Acl_Resource('admin:overview', 'admin'))
                ->add(new Zend_Acl_Resource('admin:products', 'admin'))
                ->add(new Zend_Acl_Resource('admin:settings', 'admin'))
                ->add(new Zend_Acl_Resource('admin:shipping', 'admin'))
                ->add(new Zend_Acl_Resource('admin:payment', 'admin'))                
                ->add(new Zend_Acl_Resource('admin:texts', 'admin'));
        
        // SuperAdmin module
        $this->add(new Zend_Acl_Resource('superadminadmin'))
                ->add(new Zend_Acl_Resource('admin:users', 'admin'))
                ->add(new Zend_Acl_Resource('admin:server', 'admin'))
                ->add(new Zend_Acl_Resource('admin:seo', 'admin'));
        

        // Permission
        // Guests mohou jen na defaultní modul.    
        $this->allow('guest', 'default:index');    
        $this->allow('guest', 'default:jidlo');
        $this->allow('guest', 'default:kluboveinfo');
        $this->allow('guest', 'default:eshopmaintenance');
        $this->allow('guest', 'default:kontakt');
        $this->allow('guest', 'default:menu');
        $this->allow('guest', 'default:napoje');
        $this->allow('guest', 'default:obedy');
        $this->allow('guest', 'default:pivo');
        $this->allow('guest', 'default:gallery');
        $this->allow('guest', 'default:mailinglist');
        $this->allow('guest', 'default:eshop');
        $this->allow('guest', 'default:error');
        $this->allow('guest', 'admin:mail');        
        
        // Editor může upravovat pouze piva na čepu
        $this->allow('editor', 'admin:authentication');
        $this->allow('editor', 'admin:nacepu');
        $this->allow('editor', 'admin:obedy');

        // Admin mohou na defaultní i admin modul.
        $this->allow('admin', 'admin:gallery');
        $this->allow('admin', 'admin:index');        
        $this->allow('admin', 'admin:articles');
        $this->allow('admin', 'admin:jidlo');
        $this->allow('admin', 'admin:kluboveinfo');
        $this->allow('admin', 'admin:kontakt');
        $this->allow('admin', 'admin:menu');
        $this->allow('admin', 'admin:nabidka');
        $this->allow('admin', 'admin:napoje');
        $this->allow('admin', 'admin:nealko');
        $this->allow('admin', 'admin:novinka');
        $this->allow('admin', 'admin:pivo');
        $this->allow('admin', 'admin:error');        
        $this->allow('admin', 'admin:mailinglist');  
        $this->allow('admin', 'admin:texts');
        
        //tohle se týká eshopu
        $this->allow('admin', 'admin:categories');
        $this->allow('admin', 'admin:subcategories');
        $this->allow('admin', 'admin:products');
        $this->allow('admin', 'admin:orders');  
        $this->allow('admin', 'admin:overview');       
        $this->allow('admin', 'admin:settings');               
        $this->allow('admin', 'admin:shipping');               
        $this->allow('admin', 'admin:payment');
        
        //Superadmini mohou na users modul, kde mohou přidávat další uživatele s admin oprávněním
        $this->allow('superadmin', 'admin:users');
        $this->allow('superadmin', 'admin:server');
        $this->allow('superadmin', 'admin:seo');
        
    }

}
