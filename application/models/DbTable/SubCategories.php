<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class Model_DbTable_SubCategories extends Zend_Db_Table_Abstract {

    protected $_name = 'eshop_subcategories';
    protected $_primary = 'subcategory_id';
    
    public function fetchSubcategories($category_id = null) {
        //$defaultSession = new Zend_Session_Namespace('Default');
        $db = Zend_Db_Table::getDefaultAdapter();
        $query = "SELECT es.subcategory_id, ec.title_cz AS category_text, es.sequence, es.title_cz AS title, es.alias_cz AS alias, es.alias_cz, es.alias_en, es.text_cz AS text, es.title_en, es.text_en, es.public AS public, es.type AS type
                    FROM eshop_categories AS ec JOIN eshop_subcategories AS es
                        ON ec.category_id = es.category_id";
        //$query = $this->select()->from($this->_name.' AS es', array('es.subcategory_id', 'es.category_id', 'es.sequence','es.title_'.$defaultSession->lang.' AS title', 'es.text_'.$defaultSession->lang.' AS text'));
        /*
        if (isset($category_id)) {
            $query->where('category_id = ?', $category_id);
        }*/
        if (empty($category_id)) {
            try {
                $query .= " ORDER BY es.category_id, es.sequence";
                $result = $db->fetchAll($query);
                //$result = $this->fetchAll($query)->toArray();
            } catch (Zend_Exception $e) {
                echo "Caught exception: " . get_class($e) . "\n";
                echo "Message: " . $e->getMessage() . "\n";
            }
        } else {
            try {
                $query .= " WHERE es.category_id = '$category_id'";
                $query .= " ORDER BY es.category_id, es.sequence";
                $result = $db->fetchAll($query);
                //$result = $this->fetchAll($query)->toArray();
            } catch (Zend_Exception $e) {
                echo "Caught exception: " . get_class($e) . "\n";
                echo "Message: " . $e->getMessage() . "\n";
            }
        }
        return $result;
    }
}