<?php

/**
 * Formulář k přihlášení se do administrace.
 *
 * @package default
 * @author Daniel Vála
 */
class Form_CategoriesformCZEN extends Zend_Form {

    public function __construct($action, $text = null) {
        parent::__construct($text);
        $this->setMethod('POST')->setName('login')->setAction($action);
        $this->setAttrib('class', 'admintable');
        
        //momentálně je formulář nastaven na dva jazyky (cz a en), nevím jak ho upravit pro libovolný počet jazyků z configu
        $value = !empty($text[0]['category_id']) ? $text[0]['category_id'] : "";
        $text_id = new Zend_Form_Element_Hidden('category_id', array('class' => 'nodisplay', 'value' => $value));
        
        $value = !empty($text[0]['sequence']) ? $text[0]['sequence'] : ""; 
        $sequence = new Zend_Dojo_Form_Element_TextBox('sequence', array('class' => 'textboxwide', 'value' => $value));
        $sequence->setLabel('Sekvence:')->setRequired(true);
        
        $value = !empty($text[0]['title_cz']) ? $text[0]['title_cz'] : "";
        $title_cz = new Zend_Dojo_Form_Element_TextBox('title_cz', array('class' => 'textboxwide', 'value' => $value));
        $title_cz->setLabel('Nadpis česky:')->setRequired(true);

        $value = !empty($text[0]['text_cz']) ? $text[0]['text_cz'] : "";
        $text_cz = new Zend_Dojo_Form_Element_Textarea('text_cz', array('class' => "textboxbig", 'value' => $value));
        $text_cz->setLabel('Text česky:')->setRequired(true);
        
        $value = !empty($text[0]['title_en']) ? $text[0]['title_en'] : "";
        $title_en = new Zend_Dojo_Form_Element_TextBox('title_en', array('class' => 'textboxwide', 'value' => $value));
        $title_en->setLabel('Nadpis anglicky:')->setRequired(true);

        $value = !empty($text[0]['text_en']) ? $text[0]['text_en'] : "";
        $text_en = new Zend_Dojo_Form_Element_Textarea('text_en', array('class' => "textboxbig", 'value' => $value));
        $text_en->setLabel('Text anglicky:')->setRequired(true);

        $submit = new Zend_Form_Element_Submit('submit', array('label' => "Uložit", 'class' => "savebutton"));

        $this->addElements(array(
            $sequence, $text_id, $title_cz, $text_cz, $title_en, $text_en, $submit
        ));
    }
}
