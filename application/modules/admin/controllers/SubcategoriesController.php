<?php

class Admin_SubcategoriesController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction() {        
        $subCategories = new Model_DbTable_SubCategories();
        $this->view->subcategories = $subCategories->fetchSubcategories();        
    }
    
    public function editAction() {
        $subcatmodel    = new Model_DbTable_SubCategories();
        $subcategoryId  = $this->getParam('subcategory_id');
        $subCategory    = $subcatmodel->find($subcategoryId)->toArray();
        //nasolíme text do připraveného formuláře včetně seznamu kategorií
        if (empty($subCategory)) {
            $form       = new Form_Subcategoriesform($subCategory[0]);
            $this->view->title = "nová";
        } else {
            $form       = new Form_Subcategoriesform($subCategory[0]);
            $this->view->title = $subCategory[0]['title_cz'];
        }
        $this->view->form = $form;
        $this->view->text = $subCategory;
    }
    
    
    public function saveAction() {
        $table      = new Model_DbTable_SubCategories();        
        $seo        = new Model_SeoConverter();
        $subCategoryId = $this->getParam('subcategory_id');
        $data = $this->getRequest()->getPost();
        unset($data['submit']);   
        $data['alias_cz'] = $seo->makeAlias($data['title_cz'], 'cz');
        $data['alias_en'] = $seo->makeAlias($data['title_en'], 'cz');
        if (empty($subCategoryId)) {
            $table->insert($data);
        } else {
            $where = $table->getAdapter()->quoteInto('subcategory_id = ?', $subCategoryId);
            $table->update($data, $where);
        }
        //$this->render('index');
        $this->_redirect('/admin/subcategories/');
    }
    
    public function deleteAction() {      
        $subCategoryId = $this->getParam('subcategory_id');
        if (!empty($subCategoryId)) {
            $table = new Model_DbTable_SubCategories();
            $where = $table->getAdapter()->quoteInto('subcategory_id = ?', $subCategoryId);
            $table->delete($where);
        }
        $this->_redirect('/admin/subcategories/');
    }

}

