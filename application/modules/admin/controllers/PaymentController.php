<?php
class Admin_PaymentController extends Zend_Controller_Action
{
    public function init()
    {
        /* Initialize action controller here */
    }

    function indexAction() {  
        $model                  = new Model_DbTable_EshopPayments();
        $settings               = $model->fetchAll()->toArray();
        $form                   = new Form_Paymentform($settings);
        $this->view->form       = $form;
    }
    
    function saveAction() {
        $model                  = new Model_DbTable_EshopPayments();
        $count                  = $this->getParam('count');
        for ($i = 1; $i <= $count; $i++) {
            $payment_id = $this->getParam($i.'payment_id');
            $data = array(
                'text_cz'     => trim($this->getParam($i.'text_cz')),
                'text_en'     => trim($this->getParam($i.'text_en')),
                'public'      => $this->getParam($i.'public'),
                'price_cz'       => $this->getParam($i.'price_cz')
            );   
            $where = $model->getAdapter()->quoteInto('payment_id = ?', $payment_id);
            $model->update($data, $where);
        } 
        $this->_redirect('/admin/payment/');
    }
}
?>
