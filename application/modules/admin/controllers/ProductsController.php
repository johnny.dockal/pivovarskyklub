<?php

class Admin_ProductsController extends Zend_Controller_Action {
    
    private $category_id = 'all';
    private $page = 1;
    private $order = 'product_id';
    private $sort = 'desc';
    private $limit = 50;
    
    public function init() {
        /* Initialize action controller here */
    }
    public function preDispatch() {
        $category_id = $this->getParam('category_id');
        $page = $this->getParam('page');
        $order = $this->getParam('order');
        $sort = $this->getParam('sort');
        
        $adminSession = new Zend_Session_Namespace('Admin');
        
        if (!empty($category_id)) {
            $this->category_id = $category_id;
            $adminSession->category_id = $category_id;
            //pokus se mění kategorie, musí se jít zpátky na stránku 1, aby nedošlo k out of bounds exception
            $adminSession->page = $this->page;
        } else {
            $this->category_id = $adminSession->category_id;
        }
        if (empty($adminSession->category_id)) {
            $adminSession->category_id = $this->category_id;
        }
        //nastavíme stránku
        if (!empty($page)) {
            $this->page = $page;
            $adminSession->page = $page;
        } else if (empty($adminSession->page)) {            
            $adminSession->page = $this->page;
        } else {
            $this->page = $adminSession->page;
        }  
        //nastavíme pořadí
        if (!empty($order)) {
            $this->order = $order;
            $adminSession->order = $order;
        } else {
            $this->order = $adminSession->order;
        }
        if (empty($adminSession->order)) {
            $adminSession->order = $this->order;
        }
        if (!empty($sort)) {
            $this->sort = $sort;
            $adminSession->sort = $sort;
        } else {
            $this->sort = $adminSession->sort;
        }
        if (empty($adminSession->sort)) {
            $adminSession->sort = $this->sort;
        }
        
        $this->view->category_id = $adminSession->category_id;
        $this->view->page = $adminSession->page;
        $this->view->order = $adminSession->order;
        $this->view->sort = $adminSession->sort;
    }

    public function indexAction() {
        $categories = new Model_DbTable_EshopCategories();
        $model = new Model_DbTable_EshopProducts();
        
        $products = $model->fetchProductsByCat($this->category_id, $this->order, false);       
        $this->view->categories = $categories->fetchAll()->toArray();
        $this->view->itemlimit = $this->limit;
        $this->view->itemcount = count($products);
        $this->view->pagecount = ceil($this->view->itemcount / $this->limit);        
        $this->view->products = array_chunk($products, $this->limit);
    }

    public function editAction() {
        $subcatmodel = new Model_DbTable_SubCategories();
        $productmodel = new Model_DbTable_EshopProducts();
        $relationmodel = new Model_DbTable_SubcatProducts();
        
        //podle toho do jaké kategorie chceme produkt přidat tak zvolíme subkategorie pro formulář
        $product_id = $this->getParam('product_id');
        if (empty($product_id)) {
            if (is_numeric($this->getParam('category_id'))) {
                $category_id = $this->getParam('category_id');
            } else {
                if ($this->getRequest()->getPost('predmety')) {
                    $category_id = 2;
                } else if ($this->getRequest()->getPost('homebrew')) {
                    $category_id = 3;
                } else {
                    $category_id = 1;
                }
            }
        } else {
            $product = $productmodel->fetchProductEdit($product_id);
            $category_id = $product['category_id'];
        }    
        $subcatoptions = $subcatmodel->fetchSubcategories($category_id);
        
        //uložení produktu
        if ($this->getRequest()->getPost('save')) {
            $product_id = $this->saveProduct();
            $this->_redirect('/admin/products/view/?product_id=' . $product_id);
        } else {
            //nasolíme text do připraveného formuláře včetně seznamu kategorií
            if (empty($product)) {
                $form = new Form_ProductsformCZEN('/admin/products/edit/', $subcatoptions);
                $data = array(
                    'title_cz' => $this->getParam('title_cz'),
                    'text_cz' => $this->getParam('text_cz'),
                    'title_en' => $this->getParam('title_en'),
                    'text_en' => $this->getParam('text_en'),
                    'status_cz' => $this->getParam('status_cz'),
                    'producer' => $this->getParam('producer'),
                    'size' => $this->getParam('size'),
                    'vat_rate' => $this->getParam('vat_rate'),
                    'price_cz' => $this->getParam('price_cz')
                );
                $form->populate($data);
                $this->view->title = "nový";
            } else {
                $selected = $relationmodel->fetchProductSubcats($product_id);
                $form = new Form_ProductsformCZEN('/admin/products/edit/', $subcatoptions, $selected);
                //checkboxy se subkategoriema se zvolej pomocí $selected, tohle by to přepsalo
                unset($product['subcategory_id']);
                $form->populate($product);
                $this->view->title = $product['title_cz'];
                $this->view->selected = $selected;            
            }
            $this->view->subcatoptions = $subcatoptions;
            $this->view->form = $form;
        }
    }

    public function viewAction() {
        $product_id = $this->getParam('product_id');
        $model = new Model_DbTable_EshopProducts();
        $this->view->product = $model->fetchProduct($product_id);
    }

    public function deleteAction() {
        $product_id = $this->getParam('product_id');
        if (!empty($product_id)) {
            $table = new Model_DbTable_EshopProducts();
            $where = $table->getAdapter()->quoteInto('product_id = ?', $product_id);
            $table->delete($where);
        }
        $this->_redirect('/admin/products/');
    }

    public function saveProduct() {
        $productmodel = new Model_DbTable_EshopProducts();
        $seomodel = new Model_SeoConverter();
        $product_id = $this->getParam('product_id');
        $selected = $this->getParam('subcategory_id');
        if (empty($selected)) {
            $selected = array();
        }
        //aby se nemusel vyplňovat název piva dvakrát
        $title_en = $this->getParam('title_en');
        if (empty($title_en)) {
            $title_en = $this->getParam('title_cz');
        }
        $data = array(
            'title_cz' => $this->getParam('title_cz'),
            'text_cz' => $this->getParam('text_cz'),
            'title_en' => $title_en,
            'text_en' => $this->getParam('text_en'),
            'status_cz' => $this->getParam('status_cz'),
            'producer' => $this->getParam('producer'),
            'size' => $this->getParam('size'),
            'vat_rate' => $this->getParam('vat_rate'),
            'price_cz' => $this->getParam('price_cz')
        );
        if (empty($product_id)) {
            //insertujeme produkt
            $product_id = $productmodel->saveProduct($data, $selected);
            $data2 = array();
            $data2['alias_cz'] = $seomodel->makeAlias($this->getParam('title_cz'), 'cz', $product_id);
            $data2['alias_en'] = $seomodel->makeAlias($this->getParam('title_en'), 'en', $product_id);
            $productmodel->update($data2, 'product_ud = '.$product_id);
        } else {
            //updatujeme produkt            
            $data['alias_cz'] = $seomodel->makeAlias($this->getParam('title_cz'), 'cz', $product_id);
            $data['alias_en'] = $seomodel->makeAlias($this->getParam('title_en'), 'en', $product_id);
            $productmodel->saveProduct($data, $selected, $product_id);
        }

        $form = new Form_ProductsformCZEN();
        if ($form->product_image2->isUploaded()) {
            echo "obrázek vybrán<br/>";
            $path = getcwd() . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'eshop_products' . DIRECTORY_SEPARATOR;
            $target_name = $product_id . ".jpg";
            $target_path_l = $path . "l" . DIRECTORY_SEPARATOR . $target_name;
            $target_path_m = $path . "m" . DIRECTORY_SEPARATOR . $target_name;
            $target_path_s = $path . "s" . DIRECTORY_SEPARATOR . $target_name;

            $validator = new Zend_Validate_File_Exists();
            $validator->addDirectory(getcwd());

            if ($validator->isValid($target_path_l)) {
                unlink($target_path_l);
            }
            if ($validator->isValid($target_path_m)) {
                unlink($target_path_m);
            }
            if ($validator->isValid($target_path_s)) {
                unlink($target_path_s);
            }
            
            $form->product_image2->setDestination($path);
            $form->product_image2->addFilter('Rename', $target_name);
            
            echo "teď se bude přenastavovat velikost...";
            $filterChain = new Zend_Filter();
            $filterChain->appendFilter(new Skoch_Filter_File_Resize(array(
                'directory' => $path . 'l',
                'width' => 400,
                'height' => 600,
                'cropToFit' => true,
                'keepRatio' => true,
                'keepSmaller' => false,
            )));
            $filterChain->appendFilter(new Skoch_Filter_File_Resize(array(
                'directory' => $path . 'm',
                'width' => 100,
                'height' => 100,
                'cropToFit' => true,
                'keepRatio' => true,
                'keepSmaller' => false,
            )));
            $filterChain->appendFilter(new Skoch_Filter_File_Resize(array(
                'directory' => $path . 's',
                'width' => 80,
                'height' => 80,
                'cropToFit' => true,
                'keepRatio' => true,
                'keepSmaller' => false,
            )));
            $form->product_image2->addFilter($filterChain);
            echo ini_get('upload_max_filesize');
            echo "<br/>teď se bude ukládat...";
            try {
                echo "<br/>zkusíme to uložit"; 
                if ($form->product_image2->receive()) {
                    echo "<br/>povedlo se";
                } else {
                    echo "<br/>NEpovedlo se";
                }
            } catch (Zend_File_Transfer_Exception $e) {
                throw new Exception('Unable to recieve : ' . $e->getMessage() . " <br/>PATH: " . $path);
            }
        } else {
            echo "Obrázek formátu 1*2 nebyl vybrán";
        }
        if ($form->product_image->isUploaded()) {
            echo "obrázek vybrán<br/>";
            $path = getcwd() . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'eshop_products' . DIRECTORY_SEPARATOR;
            $target_name = $product_id . ".jpg";
            $target_path_l = $path . "l" . DIRECTORY_SEPARATOR . $target_name;
            $target_path_m = $path . "m" . DIRECTORY_SEPARATOR . $target_name;
            $target_path_s = $path . "s" . DIRECTORY_SEPARATOR . $target_name;

            $validator = new Zend_Validate_File_Exists();
            $validator->addDirectory(getcwd());

            if ($validator->isValid($target_path_l)) {
                unlink($target_path_l);
            }
            if ($validator->isValid($target_path_m)) {
                unlink($target_path_m);
            }
            if ($validator->isValid($target_path_s)) {
                unlink($target_path_s);
            }
            
            $form->product_image->setDestination($path);
            $form->product_image->addFilter('Rename', $target_name);
            
            echo "teď se bude přenastavovat velikost...";
            $filterChain = new Zend_Filter();
            $filterChain->appendFilter(new Skoch_Filter_File_Resize(array(
                'directory' => $path . 'l',
                'width' => 300,
                'height' => 600,
                'cropToFit' => true,
                'keepRatio' => true,
                'keepSmaller' => false,
            )));
            $filterChain->appendFilter(new Skoch_Filter_File_Resize(array(
                'directory' => $path . 'm',
                'width' => 100,
                'height' => 200,
                'cropToFit' => true,
                'keepRatio' => true,
                'keepSmaller' => false,
            )));
            $filterChain->appendFilter(new Skoch_Filter_File_Resize(array(
                'directory' => $path . 's',
                'width' => 50,
                'height' => 50,
                'cropToFit' => true,
                'keepRatio' => true,
                'keepSmaller' => false,
            )));
            $form->product_image->addFilter($filterChain);
            echo ini_get('upload_max_filesize');
            echo "<br/>teď se bude ukládat...";
            try {
                echo "<br/>zkusíme to uložit"; 
                if ($form->product_image->receive()) {
                    echo "<br/>povedlo se";
                } else {
                    echo "<br/>NEpovedlo se";
                }
            } catch (Zend_File_Transfer_Exception $e) {
                throw new Exception('Unable to recieve : ' . $e->getMessage() . " <br/>PATH: " . $path);
            }
        } else {
            echo "Obrázek formátu 1*2 nebyl vybrán";
        }

        return $product_id;
    }

}
