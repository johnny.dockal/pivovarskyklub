<?php

class Admin_CategoriesController extends Zend_Controller_Action
{

    public function init() {
        /* Initialize action controller here */
    }

    public function indexAction() {
        $categories = new Model_DbTable_EshopCategories();
        $this->view->categories = $categories->fetchAll()->toArray();
    }
    
    public function editAction() {
        $model      = new Model_DbTable_EshopCategories();
        $categoryId = $this->getParam('category_id');
        $text       = $model->find($categoryId)->toArray();
        //nasolíme text do připraveného formuláře pro úpravu kategorií a subkategorií (pro češtinu a angličtinu)
        if (empty($text)) {
            $form       = new Form_CategoriesformCZEN('/admin/categories/save/');
            $this->view->title = "nová";
        } else {
            $form       = new Form_CategoriesformCZEN('/admin/categories/save/', $text);
            $this->view->title = $text[0]['title_cz'];
        }
        $this->view->form = $form;
        $this->view->text = $text;
    }
    
    public function saveAction() {
        $table      = new Model_DbTable_EshopCategories();        
        $categoryId = $this->getParam('category_id');
        $data = array(
            'sequence'  => $this->getParam('sequence'),
            'title_cz'  => $this->getParam('title_cz'),
            'text_cz'   => $this->getParam('text_cz'),
            'title_en'  => $this->getParam('title_en'),
            'text_en'   => $this->getParam('text_en'),
        );
        if (empty($categoryId)) {
            $table->insert($data);
        } else {
            $where = $table->getAdapter()->quoteInto('category_id = ?', $categoryId);
            $table->update($data, $where);
        }
        $this->_redirect('/admin/categories/');
    }

}

