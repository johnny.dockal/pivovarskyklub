<?php

class Model_SeoConverter {

    private $checkTables = array('eshop_categories', 'eshop_subcategories', 'eshop_products');
    private $transferTable = array(
        'ä' => 'a', 'Ä' => 'A', 'á' => 'a', 'Á' => 'A', 'à' => 'a', 'À' => 'A', 'ã' => 'a', 'Ã' => 'A', 'â' => 'a', 'Â' => 'A',
        'č' => 'c', 'Č' => 'C', 'ć' => 'c', 'Ć' => 'C', 'ď' => 'd', 'Ď' => 'D', 'ě' => 'e',
        'Ě' => 'E', 'é' => 'e', 'É' => 'E', 'ë' => 'e', 'Ë' => 'E', 'è' => 'e', 'È' => 'E', 'ê' => 'e', 'Ê' => 'E',
        'í' => 'i', 'Í' => 'I', 'ï' => 'i', 'Ï' => 'I', 'ì' => 'i', 'Ì' => 'I', 'î' => 'i', 'Î' => 'I', 'ľ' => 'l',
        'Ľ' => 'L', 'ĺ' => 'l', 'Ĺ' => 'L',
        'ń' => 'n', 'Ń' => 'N', 'ň' => 'n', 'Ň' => 'N', 'ñ' => 'n', 'Ñ' => 'N',
        'ó' => 'o', 'Ó' => 'O', 'ö' => 'o', 'Ö' => 'O', 'ô' => 'o', 'Ô' => 'O', 'ò' => 'o', 'Ò' => 'O', 'õ' => 'o', 'Õ' => 'O', 'ő' => 'o', 'Ő' => 'O',
        'ř' => 'r', 'Ř' => 'R', 'ŕ' => 'r', 'Ŕ' => 'R',
        'š' => 's', 'Š' => 'S', 'ś' => 's', 'Ś' => 'S',
        'ť' => 't', 'Ť' => 'T',
        'ú' => 'u', 'Ú' => 'U', 'ů' => 'u', 'Ů' => 'U', 'ü' => 'u', 'Ü' => 'U', 'ù' => 'u', 'Ù' => 'U', 'ũ' => 'u', 'Ũ' => 'U', 'û' => 'u', 'Û' => 'U',
        'ý' => 'y', 'Ý' => 'Y',
        'ž' => 'z', 'Ž' => 'Z', 'ź' => 'z', 'Ź' => 'Z'
    );

    function makeAlias($url, $checklang = null, $id = null, $limit = 75) {
        $url = trim($url);
        $url = strtr($url, $this->transferTable);
        $url = preg_replace('~[^\\pL0-9_]+~u', '-', $url);
        $url = trim($url, "-");
        $url = iconv("utf-8", "us-ascii//TRANSLIT", $url);
        $url = strtolower($url);
        $url = preg_replace('~[^-a-z0-9_]+~', '', $url);

        if (isset($id)) {
            $url = "$id-" . $url;
        }
        if (strlen($url) > $limit) {
            $url = substr($url, 0, $limit);
        }
        if (empty($url)) {
            //return 'n-a';
            return time();
        }
        if ($checklang) {
            while ($this->checkDuplicateAlias($url, $checklang)) {
                $url .= '-1';
            }
        }
        return $url;
    }
    
    private function checkDuplicateAlias($url, $checklang) {
        $db = Zend_Db_Table::getDefaultAdapter();
        $duplicate = false;
        foreach ($this->checkTables as $value) {
            $select = $db->select()->from($value, array("alias_$checklang"))
                ->where("alias_$checklang = '$url'");
            $result = $db->fetchRow($select);
            if ($result) {
                $duplicate = true;
            }
        }
        return $duplicate;
    }

}
