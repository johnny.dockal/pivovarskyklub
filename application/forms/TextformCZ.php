<?php

/**
 * Formulář k přihlášení se do administrace.
 *
 * @package default
 * @author Daniel Vála
 */
class Form_TextformCZ extends Zend_Form {

    public function __construct($text, $action = '/admin/texts/save/') {
        parent::__construct();
        $this->setMethod('POST')->setName('login')->setAction($action);
        $this->setAttrib('class', 'admintable');
        
        //momentálně je formulář nastaven na dva jazyky (cz a en), nevím jak ho upravit pro libovolný počet jazyků z configu
        $text_id = new Zend_Form_Element_Hidden('text_id');
        //zabání zobrazení labelu
        $text_id->removeDecorator('label');
        
        $title_cz = new Zend_Dojo_Form_Element_TextBox('title_cz', array('class' => 'textboxwide'));
        $title_cz->setLabel('Nadpis česky:')->setRequired(true);
        
        $title_en = new Zend_Dojo_Form_Element_TextBox('title_en', array('class' => 'textboxwide'));
        $title_en->setLabel('Nadpis anglicky:')->setRequired(true);

        $text_cz = new Zend_Dojo_Form_Element_Textarea('text_cz', array('class' => "textboxhuge"));
        $text_cz->setLabel('Text česky:')->setRequired(true);

        $submit = new Zend_Form_Element_Submit('submit', array('label' => "Uložit", 'class' => "savebutton"));

        $this->addElements(array(
            $text_id, $title_cz, $title_en, $text_cz, $submit
        ));
    }
}
