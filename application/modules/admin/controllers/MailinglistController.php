<?php

class Admin_MailinglistController extends Zend_Controller_Action {

    public function init() {
        
    }

    public function indexAction() {
        $mailmodel = new Model_DbTable_Mailinglist();
        $recipients = $mailmodel->fetchAll();
        $form = new Form_Mailinglistform('/admin/mailinglist/save/');

        $this->view->form = $form;
        $this->view->recipients = $recipients;
    }

    public function saveAction() {
        $mailmodel = new Model_DbTable_Mailinglist();
        $address = $this->getParam('address');        
        if (isset($address)) {
            //zkontrolujeme, zda už náhodou není adresa v databázi
            $entry = $mailmodel->fetchRow("mail = '$address'");
            if (isset($entry)) {
                //adresa je v databázi, ale je odhlášená od odběru
                if ($entry->status < 0) {
                    $data = array(
                        'status' => '5'
                    );
                    $where = "mail_id = '$entry->mail_id'";
                    $mailmodel->update($data, $where);
                    $this->view->message = "<h2>Adresa $address úspěšně obnovena (změněn status).</h2>";
                } else {
                    $this->view->message = "<h2>Tuto adresu ($address) již máme v databázi.</h2>";
                }
            } else {
                $data = array(
                    'mail'      => $address,
                    'status'    => '2',
                    'token'     => md5($address."a1b2c3")
                );
                $mailmodel->insert($data);
                $this->view->message = "<h2>Adresa $address úspěšně registrována. Děkujeme vám za důvěru.</h2>";
            }
        } else {
            $this->view->message = "<h2>Adresa není zadána správně.</h2>";
        }
        
        $this->indexAction();
        $this->render('index');
    }

    public function deleteAction() {
        $mailmodel = new Model_DbTable_Mailinglist();
        $data = array(
            'status' => '-2'
        );
        $where = "mail_id = '" . $this->getParam('mail_id') . "'";
        $mailmodel->update($data, $where);
        $this->_redirect('/admin/mailinglist/');
    }

}

