<?php

class Admin_ArticlesController extends Zend_Controller_Action {

    public function init() {
        $this->view->headTitle('Články - Klubové akce', 'POSTEND');
    }

    public function indexAction() {
        $status_public = $this->_getParam('public');
        $status_private = $this->_getParam('private');
        $status_deleted = $this->_getParam('deleted');

        $articleModel = new Model_DbTable_Articles();
        $select = $articleModel->select();
        if ($status_public == 1) {
            $select->orwhere('status = ?', 'public');
        }
        if ($status_private == 1) {
            $select->orwhere('status = ?', 'private');
        }
        if ($status_deleted == 1) {
            $select->orwhere('status = ?', 'deleted');
        }
        $this->view->articles = $articleModel->fetchAll($select);
    }

    public function editAction() {        
        $article_id = $this->_getParam('article_id');
        if ($article_id == 'new') {
            $form = new Form_Articleform('/admin/articles/save/');
        } else {
            $articleModel = new Model_DbTable_Articles();
            $article = $articleModel->find($article_id);
            $categoryModel = new Model_DbTable_Categories();
            $categories = $categoryModel->fetchAll();
            $form = new Form_Articleform('/admin/articles/save/', $article, $categories);
        }
        //nasolíme text do připraveného formuláře (pro jeden jazyk)
        $this->view->form = $form;
    }

    public function saveAction() {
        $articleModel = new Model_DbTable_Articles();
        $article_id = $this->_getParam('article_id');
        $data = array(
            'category_id'   => $this->getParam('article_category_id'),
            'title'         => $this->getParam('article_title'),
            'date'          => $this->getParam('article_date'),
            'status'        => $this->getParam('article_status'),
            'perex'         => $this->getParam('article_perex'),
            'text'          => $this->getParam('article_text')
        );
        if (empty($article_id)) {
            $articleModel->insert($data);
        } else {
            $where = $articleModel->getAdapter()->quoteInto('article_id = ?', $article_id);
            $articleModel->update($data, $where);
        }
        
        $form = new Form_Articleform('/admin/articles/save/');
        if ($form->article_image->isUploaded()) {
            $path = getcwd().DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.'articles';
            $target_path = $path.DIRECTORY_SEPARATOR.$article_id.".jpg";   
            if (move_uploaded_file($_FILES['article_image']['tmp_name'], $target_path)) {
                echo "The file $target_path has been uploaded";
            } else{
                echo "Chyba při načítání obrázku, zkus to znovu!";
            }
        }
        $this->_redirect('/admin/articles/');
    }
}

