<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class Model_DbTable_OrderDetails extends Zend_Db_Table_Abstract {

    protected $_name = 'eshop_order_details';
    protected $_primary = 'order_detail_id';
    
    public function saveOrderDetails($order_id, $data) {
        foreach ($data as $product) {
            try {
                $this->insert(array(
                    'order_id'          => $order_id, 
                    'product_id'        => $product->getProductId(), 
                    'product_title'     => $product->getTitle(),
                    'product_producer'  => $product->getProducer(),
                    'product_size'      => $product->getSize(),
                    'product_price'     => $product->getPrice(),
                    'product_quantity'  => $product->getQuantity()
                ));
            } catch (Zend_Exception $e) {
                echo "Caught exception during saving order details: " . get_class($e) . "\n";
                echo "Message: " . $e->getMessage() . "\n";
            }
        }
        
        return 1;
    } 
}