<?php
class Admin_SettingsController extends Zend_Controller_Action
{
     public function init()
    {
        /* Initialize action controller here */
    }

    function indexAction() {
        $form                   = new Form_Settingsform();
        $modelSettings          = new Model_DbTable_Settings();
        $settings               = $modelSettings->fetchAll()->toArray();
        $this->view->form       = $form->populate($settings);
    }
    
    function saveAction() {
        $modelSettings          = new Model_DbTable_Settings();
        $setting_id             = $this->getParam('setting_id');
        $data = array(
            'setting_value1'     => $this->getParam('setting_value1'),
            'setting_value2'     => $this->getParam('setting_value2'),
            'setting_text_cz'       => trim($this->getParam('setting_text_cz')),
            'setting_text_en'       => trim($this->getParam('setting_text_en'))
        );
        $where = $modelSettings->getAdapter()->quoteInto('setting_id = ?', $setting_id);
        $modelSettings->update($data, $where);
        $this->_redirect('/admin/settings/');
    }
}
?>
