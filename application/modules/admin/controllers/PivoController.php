<?php

class Admin_PivoController extends Zend_Controller_Action
{
    private $action;
    
    public function init() {
        $this->view->headTitle('Nabídka piv', 'POSTEND');
        $this->action = $this->_getParam('view');  
        if (empty($this->action)) {
            $this->action = 'lager';
        }
        $this->view->action = $this->action;
    }

    public function indexAction() {
        //vylovíme daný text v mnohojazyčné podobě
        $model = new Model_DbTable_Texts();
        $text  = $model->find($this->action)->toArray();
        $form  = new Form_TextformCZEN('/admin/pivo/save/');        
        //nasolíme text do připraveného formuláře (pro češtinu a angličtinu)
        $form->populate($text[0]);
        $this->view->form = $form;
    }

    public function saveAction() {
        $table = new Model_DbTable_Texts();
        $table->updateText();
        $this->_redirect('/admin/pivo/');
    }

}

