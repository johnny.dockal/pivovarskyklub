<?php
class Admin_NealkoController extends Zend_Controller_Action {
    
    private $text           = 'nealko';
    private $text_backup    = 'nealko-zaloha';

    public function init() {
        $this->view->headTitle('Nabídka nealkoholických nápojů', 'POSTEND');
    }

    public function indexAction() {
        $form = new Form_TextformCZ('/admin/nealko/save/');
        $model = new Model_DbTable_Texts;
        $text = $model->find('nealko')->toArray();
        $form->populate($text[0]);
        $this->view->backup_date = $model->fetchLastUpdate($this->text_backup);
        $this->view->form = $form;
    }
    
    public function backupAction() {
        $form = new Form_TextformCZ('/admin/nealko/save/');
        $model = new Model_DbTable_Texts;
        $text = $model->find('nealko-zaloha')->toArray();
        $form->populate($text[0]);
        $this->view->form = $form;
        $this->view->backup_date = $model->fetchLastUpdate($this->text_backup);
        $this->render('index');
    }

    public function saveAction() {
        $table = new Model_DbTable_Texts();
        $table->updateText();        
        $this->_redirect('/admin/nealko/');
    }


}

