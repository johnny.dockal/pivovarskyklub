<?php

class Default_NapojeController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }
    
    public function indexAction() {
        $action = $this->_getParam('view');  
        if (empty($action)) {
            $action = 'pivovice';
        }
        $this->view->action = $action;
    }

    public function palenkyAction()
    {
        $this->_helper->layout->disableLayout();
        $menu = "napoje/palenky";
        $this->_helper->viewRenderer($menu, null, true);
    }
    
    public function ostatniAction()
    {
        $this->_helper->layout->disableLayout();
        $menu = "napoje/ostatni";
        $this->_helper->viewRenderer($menu, null, true);
    }
    
    public function nealkoAction()
    {
        $this->_helper->layout->disableLayout();
        $model = new Model_DbTable_Texts();
        $text  = $model->find('nealko')->toArray();
        $this->view->text = $text[0]['text_cz'];
    }
    
    public function pivoviceAction()
    {
        $this->_helper->layout->disableLayout();
        $menu = "napoje/pivovice";
        $this->_helper->viewRenderer($menu, null, true);
    }

}

