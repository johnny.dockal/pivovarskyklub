<?php

class Admin_ObedyController extends Zend_Controller_Action
{

    public function init() {
        $this->view->headTitle('Obědy', 'POSTEND');
    }

    public function indexAction() {
        $change     = $this->_getParam('change');
        $date       = $this->_getParam('date'); 
        if (!isset($date)) {
            $date = date('Y-m-d'); 
        }
        if (isset($change)) {          
            $year   = substr($date, 0, 4);
            $day    = substr($date, -2, 2);
            $month  = substr($date, 5, 2);
            $i      = 7 * $change;            
            $date   = date('Y-m-d', mktime(0,0,0,$month,$day+$i,$year));
        }
        //vyplníme formulář již připravenými informacemi
        $table  = new Model_DbTable_Menus();
        $menudata   = $table->fetchMenus($date);
        
        $modelTexts = new Model_DbTable_Texts();
        $texts = $modelTexts->fetchTexts();
        $akce = $texts['akce']['text'];
        
        $form       = new Form_Menuform('/admin/obedy/save/', $menudata, $date, $akce);
        
        $this->view->date = $date;
        $this->view->form = $form;
    }

    public function saveAction() {
        $table      = new Model_DbTable_Menus();
        $weekArray  = array('monday', 'tuesday', 'wednesday', 'thursday', 'friday');
        $date       = $this->_getParam('date'); 
        foreach ($weekArray as $day) {
            $id = $this->getParam($day.'_id');
            if ($table->fetchRow("menu_id = '$id'")) {
                $data = array(
                    'menu_cz'      => $this->getParam($day.'_cz')
                );
                //var_dump($data);
                $where = $table->getAdapter()->quoteInto('menu_id = ?', $id);
                $table->update($data, $where);
            } else {
                $data = array(
                    'menu_id'     => $id,
                    'menu_cz'     => $this->getParam($day.'_cz')
                );
                //var_dump($data);
                $table->insert($data);
            }
        }
        //uložíme doprovodný text k menu (akce atd.)
        $modelTexts = new Model_DbTable_Texts();
        $menutext   = $this->_getParam('akce');
        $modelTexts->updateAkce($menutext);
        
        $this->_redirect('/admin/obedy/?date='.$date);
    }
}
