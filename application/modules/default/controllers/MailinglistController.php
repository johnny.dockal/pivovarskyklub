<?php

class Default_MailinglistController extends Zend_Controller_Action {

    public function init() {
        
    }

    public function indexAction() {
        $form = new Form_Mailinglistform('/mailinglist/save/');
        $this->view->form = $form;
    }

    public function saveAction() {
        $mailmodel = new Model_DbTable_Mailinglist();
        $address = $this->_request->getPost('address');
        if (isset($address)) {
            $status = 1;
        } else {
            $address    = $this->_request->getQuery('address');            
            $token      = $this->_request->getQuery('token');
            if ($token == md5($address."a1b2c3")) {
                $status = 3;                
            } else {
                $status = 0;
            }
        }
        //zkontrolujeme, zda je adresa relevantní
        if (!filter_var($address, FILTER_VALIDATE_EMAIL)) {
            $status = 0;
        }
        /*/*zkontrolujeme, zda má adresa existrující MX záznam
        if (!checkdnsrr($domain, 'MX')) {
            $status = 0;
        }*/
        if ($status > 0) {
            //zkontrolujeme, zda už náhodou není adresa v databázi
            $entry = $mailmodel->fetchRow("mail = '$address'");
            if (isset($entry)) {
                //adresa je v databázi, ale je odhlášená od odběru
                if ($entry->status <= 0) {
                    $data = array(
                        'status' => '4'
                    );
                    $where = "mail_id = '$entry->mail_id'";
                    $mailmodel->update($data, $where);
                    $this->view->message = "<h2>Adresa $address úspěšně registrována. Děkujeme vám za důvěru..</h2>";
                } else {
                    $this->view->message = "<h2>Tuto adresu ($address) již máme v databázi.</h2>";
                }
            } else {
                $data = array(
                    'mail'      => $address,
                    'status'    => $status,
                    'token'     => md5($address . "a1b2c3")
                );
                $mailmodel->insert($data);
                $this->view->message = "<h2>Adresa $address úspěšně registrována. Děkujeme vám za důvěru.</h2>";
            }
        } else {
            $this->view->message = "<h2>Adresa není zadána správně.</h2>";        
            $form = new Form_Mailinglistform('/mailinglist/save/');
            $this->view->form = $form;
        }
    }

    public function deleteAction() {
        $mailmodel  = new Model_DbTable_Mailinglist();
        $mail_id    = $this->getParam('mail_id');
        $token      = $this->getParam('token');
        
        if (isset($token)) {
            $where = "token = '$token'";
            $entry = $mailmodel->fetchRow($where);
            $this->view->mail_id    = $entry->mail_id;
            $this->view->mail       = $entry->mail;
            $this->render('confirm');
        } else if (isset($mail_id)) {
            $where = "mail_id = '$mail_id'";
            $entry = $mailmodel->fetchRow($where);
            $data = array(
                'status' => '-1'
            );
            $mailmodel->update($data, $where);
            $this->view->message = "<h2>Adresa ($entry->mail) odstraněna z databáze.</h2>";
        }
    }

}

