<?php

class Admin_OrdersController extends Zend_Controller_Action {

    private $adminSession = null;

    public function init() {
        $this->adminSession = new Zend_Session_Namespace('Admin');
        
        if (!isset($this->adminSession->year)) {
            $this->adminSession->year = date('Y');
        }
        if (!isset($this->adminSession->filter)) {
            $this->adminSession->filter = array(
                '0' => null,
                '1' => '1',
                '2' => '2',
                '3' => '3',
                '4' => '4',
                '5' => '5',
                '6' => '6',
                '7' => null,
                '8' => null
            );
        }
    }
        
    public function preDispatch() {
        $status = new Model_DbTable_EshopOrderStatus();
        $statusArray = $status->fetchAll('status_id >= 0')->toArray();

        $this->view->status = $statusArray;
        $this->view->year = $this->adminSession->year;
        $this->view->filter = $this->adminSession->filter;
    }
    
    public function indexAction() {
        $model = new Model_DbTable_EshopOrders();
        $this->view->orders = $model->fetchOrders($this->adminSession->year, $this->adminSession->filter);
    } 
        
    public function filterAction() {
        $this->adminSession->year = $this->getParam('year');
        $this->adminSession->filter = array(
            '0' => $this->getParam('0'),
            '1' => $this->getParam('1'),
            '2' => $this->getParam('2'),
            '3' => $this->getParam('3'),
            '4' => $this->getParam('4'),
            '5' => $this->getParam('5'),
            '6' => $this->getParam('6'),
            '7' => $this->getParam('7'),
            '8' => $this->getParam('8')
        );
        $this->_redirect('/admin/orders/');
    }
    
    public function editAction() {
        $order_id = $this->getParam('order_id');
        $order = new Model_EshopOrder($order_id);
        $this->view->order = $order;
        $history = new Model_DbTable_EshopOrderHistory();
        $this->view->orderHistory = $history->fetchOrderHistory($order_id);
        $status = new Model_DbTable_EshopOrderStatus();
        $statuses = $status->fetchAll('status_id >= 0')->toArray();
        unset($statuses['8']);
        unset($statuses['3']);
        $status_id = $order->getStatusId();       
        switch ($status_id) {
            case '0':
            case '1':
            case '2':
            case '3':
            case '4':
                unset($statuses['1']);
                unset($statuses['4']);
                unset($statuses['2']);
                if ($order->getShippingId() == '1' || $order->getShippingId() == '3') {
                    unset($statuses['5']);
                } else {
                    unset($statuses['6']);                    
                }
                unset($statuses['7']);
                break;                
            case '5':
            case '6':                
                unset($statuses['1']);
                unset($statuses['2']);
                unset($statuses['3']);
                unset($statuses['4']);
                unset($statuses['5']);
                unset($statuses['6']);
                break;
            case '7':
                unset($statuses['1']);
                unset($statuses['2']);
                unset($statuses['3']);
                unset($statuses['4']);
                unset($statuses['5']);
                unset($statuses['6']);
                unset($statuses['7']);                
                break;
        }
        $this->view->status = $statuses;
    }
    
    public function invoiceAction() {
        $order_id = $this->getParam('order_id'); 
        $model = new Model_EshopOrder();
        $model->fetchOrder($order_id);
        $currency = new Model_CurrencyKoruna();
        $this->view->currency = $currency;
        $this->view->order = $model;
        $this->view->layout()->disableLayout();
    }
    
    public function statusAction() {        
        $order_id = $this->getParam('order_id');
        $status_id = $this->getParam('status_id');  
        $mailaction = $this->getParam('mailaction');
        $redirect = $this->getParam('redirect');
        
        $model = new Model_DbTable_EshopOrders();
        //pokud se jedná o změnu statusu na 3 nebo 4, je třeba odeslat mail o připravené či odeslané zásilce
        switch ($status_id) {
            //týká se odesílání mailů
            case -1:
                $model->changeStatus($order_id, $status_id);
                $model->changeStatus($order_id, abs($status_id));
                break;
            case 0:
                $model->changeStatus($order_id, $status_id);
                break;
            case 2:
            case 3:
                    $model->changeStatus($order_id, $status_id);
                break;
            //pokud se jedná o změnu statusu na 4 nebo 5, je třeba odeslat mail o připravené či odeslané zásilce
            case 5:
            case 6:
                if ($mailaction == 'sent') {
                    $model->changeStatus($order_id, '-2');
                    $model->changeStatus($order_id, $status_id);
                } else if ($mailaction == 'skip') {
                    $model->changeStatus($order_id, $status_id);
                } else {
                    $this->_redirect('/admin/mail/confirm/?order_id=' . $order_id . '&status_id=' . $status_id);
                }
                break;
            case -3:
            case -4:            
            case -5:
            case -6:    
                $model->changeStatus($order_id, '-2');
                $model->changeStatus($order_id, abs($status_id));
                break;
            default:
                $model->changeStatus($order_id, $status_id);
                break;
        }      
        if (!empty($redirect)) {
            $this->_redirect('/admin/'.$redirect.'?order_id='.$order_id);
        } else {
            $this->_redirect('/admin/orders/');
        }
        // echo '/admin/'.$redirect.'?order_id='.$order_id;
    }

}
