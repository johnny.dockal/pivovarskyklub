<?php

class Model_ImageHandler {
    
    protected $gallerypath = "/images/gallery/";

    public function fetchGalleryImages() {
        $imgDir = dir(getcwd().$this->gallerypath);
        $imgFiles = array();
        while($soubor=$imgDir->read()) {
            if ($soubor=="." || $soubor==".." || $soubor=="Thumbs.db") continue;
            array_push($imgFiles, $soubor);
        }
        $imgDir->close();   
        return $imgFiles;
    }
    
    public function deleteImages() {
        
    }


}

