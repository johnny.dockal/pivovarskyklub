<?php

/**
 * Formulář k přihlášení se do administrace.
 *
 * @package default
 * @author Daniel Vála
 */
class Form_Login extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);
        $this->setMethod('POST')->setName('login');
        $this->setAttrib('class', 'admintable');

        $username = new Zend_Form_Element_Text('username');
        $username->setLabel('Uživatelské jméno:')->setRequired(true);

        $password = new Zend_Form_Element_Password('password');
        $password->setLabel('Heslo:')->setRequired(true);

        $submit = new Zend_Form_Element_Submit('submit');
        $submit->setLabel('Příhlásit se')->setIgnore(true);

        $this->addElements(array(
            $username, $password, $submit
        ));
    }

}
