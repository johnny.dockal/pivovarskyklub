<?php

/**
 * Formulář k přihlášení se do administrace.
 *
 * @package default
 * @author Daniel Vála
 */
class Form_Userform extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);
        $this->setMethod('POST')->setName('adduser')->setAction('/admin/users/save/');
        $this->setAttrib('class', 'admintable');

        $login = new Zend_Form_Element_Text('user_login');
        $login->setLabel('Uživatelský login:')->setRequired(true);

        $password = new Zend_Form_Element_Password('user_pswd');
        $password->setLabel('Heslo:')->setRequired(true);
        
        $password2 = new Zend_Form_Element_Password('pswd_check');
        $password2->setLabel('Heslo (zopakovat):')->setRequired(true);
        
        $email = new Zend_Form_Element_Text('user_email');
        $email->setLabel('Email:')->setRequired(true);
        
        $username = new Zend_Form_Element_Text('user_name');
        $username->setLabel('Uživatelské jméno (celé):')->setRequired(true);
        
        $userrole = new Zend_Form_Element_Radio('user_role');
        $userrole->setLabel('Uživatelská práva:')->setRequired(true);
        $userrole->addMultiOption('admin', 'admin');
        $userrole->addMultiOption('superadmin', 'superadmin');
        $userrole->setSeparator(' ')->setValue('admin');
        
        $submit = new Zend_Form_Element_Submit('submit', array('label' => 'Přidat uživatele', 'class' => 'savebutton'));
        $submit->setIgnore(true);

        $this->addElements(array(
            $login, $password, $password2, $email, $username, $userrole, $submit
        ));
    }

}
