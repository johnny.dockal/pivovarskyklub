<?php
/**
 * 
 */
class Form_OrderStep1form extends Zend_Form {

    public function __construct($options) {
        parent::__construct($options);
        //načteme view, ve kterém jsou již připravené texty v patřičném jazyce        
        $view = Zend_Layout::getMvcInstance()->getView(); 
        if ($view->project == 'pivoklub') {
            $this->setMethod('POST')->setName('login')->setAction('/eshop/cart/order/?step=2');
        } else {
            $this->setMethod('POST')->setName('login')->setAction('/cart/order/?step=2');
        }
        $this->setAttrib('class', 'orderform');               
       
        $order_name = new Zend_Dojo_Form_Element_TextBox('order_name', array('class' => 'textbox'));
        $order_name->setAttrib('maxlength','255');
        $order_name->setLabel($view->form_name)->setRequired(true);
        
        $order_surname = new Zend_Dojo_Form_Element_TextBox('order_surname', array('class' => 'textbox'));
        $order_surname->setAttrib('maxlength','255');
        $order_surname->setLabel($view->form_surname)->setRequired(true);

        $order_phone = new Zend_Dojo_Form_Element_TextBox('order_phone', array('class' => "textbox", 'value' => '+420'));
        $order_phone->setAttrib('maxlength','16');
        $order_phone->setLabel($view->form_phone)->setRequired(true);
        
        $order_email = new Zend_Dojo_Form_Element_TextBox('order_email', array('class' => 'textbox', 'value' => '@'));
        $order_email->setAttrib('maxlength','255');
        $order_email->setLabel($view->form_email)->setRequired(true);

        $order_address = new Zend_Dojo_Form_Element_TextBox('order_address', array('class' => "textbox"));
        $order_address->setAttrib('maxlength','255');
        $order_address->setLabel($view->form_address)->setRequired(true);
        
        $order_city = new Zend_Dojo_Form_Element_TextBox('order_city', array('class' => "textbox"));
        $order_city->setAttrib('maxlength','255');
        $order_city->setLabel($view->form_city)->setRequired(true);
        
        $order_zip = new Zend_Dojo_Form_Element_TextBox('order_zip', array('class' => "textbox"));
        $order_zip->setAttrib('maxlength','6');
        $order_zip->setLabel($view->form_zip)->setRequired(true);
        
        $country_id = new Zend_Form_Element_Select('country_id', array('class' => 'textbox'));
        $country_id->setLabel($view->form_country)->setRequired(true);        
        
        if ($view->project == 'pivoklub') {  
            $order_adult = new Zend_Form_Element_Checkbox('order_adult', array('class' => 'float-left', 'value' => '1'));
            $order_adult->setLabel("Prohlašuji, že mi již bylo 18 let.")->setRequired(true)->setChecked(false);
        }
        
        $order_agree = new Zend_Form_Element_Checkbox('order_agree', array('class' => 'float-left', 'value' => '1'));
        $order_agree->setLabel($view->str_order_agree2)->setRequired(true)->setChecked(false);
        
        foreach ($options as $value) {
           $country_id->addMultiOption($value['country_id'], $value['title']);
        }

        $submit = new Zend_Form_Element_Submit('submit', array('label' => $view->str_order_continue, 'class' => "button active cufon"));
        
        if ($view->project == 'pivoklub') {
            $this->addElements(array(
                $order_name, $order_surname, $order_phone, $order_email, $order_address, $order_city, $order_zip, $country_id, $order_adult, $order_agree, $submit
            ));
        } else {
            $this->addElements(array(
                $order_name, $order_surname, $order_phone, $order_email, $order_address, $order_city, $order_zip, $country_id, $order_agree, $submit
            ));
        }
    }
}
