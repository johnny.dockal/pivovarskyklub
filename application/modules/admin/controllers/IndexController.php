<?php

class Admin_IndexController extends Zend_Controller_Action
{

    public function init() {
        $this->view->headTitle('Úvodní text', 'POSTEND');
    }

    public function indexAction() {
        //vylovíme daný text v mnohojazyčné podobě
        $model = new Model_DbTable_Texts();
        $text  = $model->find('uvod')->toArray();
        $form  = new Form_Textform($text[0], '/admin/index/save/');
        $this->view->form = $form;
    }

    public function saveAction() {
        $table = new Model_DbTable_Texts();
        $table->updateText();
        $this->_redirect('/admin/');
    }
}

