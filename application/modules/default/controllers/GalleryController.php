<?php

/**
 * Index controller pro administrátorské rozhraní.
 *
 * @package admin
 * @author Daniel Vála
 */
class Default_GalleryController extends Zend_Controller_Action {

    public function init() {
    }

    public function indexAction() {
        $gallery = new Model_ImageHandler();
        $imgFiles = $gallery->fetchGalleryImages();
        $this->view->imgFiles = $imgFiles;
    }
    
    public function interierAction() {
        
    }
    
}
