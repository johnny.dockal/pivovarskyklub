<?php
class Admin_ShippingController extends Zend_Controller_Action
{
    public function init()
    {
        /* Initialize action controller here */
    }

    function indexAction() {       
        $model                  = new Model_DbTable_EshopShipping();
        $settings               = $model->fetchAll()->toArray();
        $form                   = new Form_Shippingform($settings);
        $this->view->form       = $form;
    }
    
    function saveAction() {
        $model                  = new Model_DbTable_EshopShipping();
        $count                  = $this->getParam('count');
        for ($i = 1; $i <= $count; $i++) {
            $shipping_id = $this->getParam($i.'shipping_id');
            $data = array(
                'text_cz'     => trim($this->getParam($i.'text_cz')),
                'text_en'     => trim($this->getParam($i.'text_en')),
                'public'      => $this->getParam($i.'public'),
                'price'       => $this->getParam($i.'price')
            );
            $where = $model->getAdapter()->quoteInto('shipping_id = ?', $shipping_id);
            $model->update($data, $where);
        }        
        $this->_redirect('/admin/shipping/');
    }
}
?>
