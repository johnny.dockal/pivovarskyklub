<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Model_DbTable_EshopProducts extends Zend_Db_Table_Abstract {

    protected $_name = 'eshop_products';
    protected $_primary = 'product_id';
    protected $_limit = 30;
    protected $_productQuery_grouped = null;
    protected $_productQuery = null;
    protected $_productQueryEdit = null;
    private $lang = null;
    private $lang_cat_subcat = null;

    public function init() {
        $session = new Zend_Session_Namespace('Default');
        $this->lang = $session->lang;
        $this->locale = $session->locale;
        $this->_productQuery_grouped = 
                        "SELECT     ep.product_id, 
                                    ep.status_$this->locale AS status,
                                    ep.vat_rate,    
                                    GROUP_CONCAT(es.category_id SEPARATOR ';') AS category_id,
                                    GROUP_CONCAT(es.subcategory_id SEPARATOR ';') AS subcategory_id,
                                    GROUP_CONCAT(es.title_cz SEPARATOR ';') AS subcategory_title,
                                    es.type AS type,
                                    ep.alias_cz AS alias,
                                    ep.title_cz AS title,
                                    ep.text_cz AS text,
                                    ep.producer,
                                    ep.size,                                    
                                    ep.price_$this->locale AS price,
                                    ec.category_id,
                                    ec.title_cz AS category_title,
                                    ec.text_cz AS category_text
                        FROM eshop_products AS ep JOIN (eshop_subcat_products AS esp, eshop_subcategories AS es, eshop_categories AS ec) 
                            ON (esp.product_id = ep.product_id AND es.subcategory_id = esp.subcategory_id AND ec.category_id = es.category_id) ";
        $this->_productQuery = 
                        "SELECT     ep.product_id, 
                                    ep.status_$this->locale AS status,
                                    ep.vat_rate,  
                                    es.subcategory_id,
                                    es.title_cz AS subcategory_title,
                                    es.type,
                                    ep.alias_cz AS alias,
                                    ep.title_cz AS title,
                                    ep.text_cz AS text,
                                    ep.producer,
                                    ep.size,                                    
                                    ep.price_$this->locale AS price,
                                    ec.category_id,
                                    ec.title_cz AS category_title,
                                    ec.text_cz AS category_text
                        FROM eshop_products AS ep LEFT JOIN (eshop_subcat_products AS esp, eshop_subcategories AS es, eshop_categories AS ec) 
                            ON (esp.product_id = ep.product_id AND es.subcategory_id = esp.subcategory_id AND ec.category_id = es.category_id) ";
        $this->_productQueryEdit = 
                        "SELECT     ep.product_id, 
                                    ep.status_cz,
                                    ep.vat_rate,  
                                    es.subcategory_id,
                                    es.type,
                                    es.title_cz AS subcategory_title,
                                    ep.alias_cz AS alias,
                                    ep.title_cz,
                                    ep.title_en,
                                    ep.text_cz,
                                    ep.text_en,
                                    ep.producer,
                                    ep.size,
                                    ep.price_cz,
                                    ec.category_id,
                                    ec.title_cz AS category_title,
                                    ec.text_cz AS category_text
                        FROM eshop_products AS ep LEFT JOIN (eshop_subcat_products AS esp, eshop_subcategories AS es, eshop_categories AS ec) 
                            ON (esp.product_id = ep.product_id AND es.subcategory_id = esp.subcategory_id AND ec.category_id = es.category_id) ";
    }

    public function fetchProduct($product_id) {
        $db = Zend_Db_Table::getDefaultAdapter();
        try {
            $query = $this->_productQuery_grouped . "WHERE ep.product_id = $product_id GROUP BY ep.product_id ";
            $product = $db->fetchRow($query);
        } catch (Zend_Exception $e) {
            echo "Caught exception " . __METHOD__ . ": " . get_class($e) . "\n <br/>";
            echo "\n <br/>Message: " . $e->getMessage() . "\n <br/>";
            echo "\n <br/>SQL: " . $sql . "\n <br/>";
        }
        //Pokud napoprvé není žádný výsledek, může se jednat o produkt, který není zařazen do žádné subkategorie
        if (empty($product)) {
            try {
                $query = $this->_productQuery . "WHERE ep.product_id = $product_id";
                $product = $db->fetchRow($query);
            } catch (Zend_Exception $e) {
                echo "Caught exception: " . get_class($e) . "\n";
                echo "Message: " . $e->getMessage() . "\n";
            }
        }
        return $product;
    }

    public function fetchProductEdit($product_id) {
        $db = Zend_Db_Table::getDefaultAdapter();
        try {
            $query = $this->_productQueryEdit . "WHERE ep.product_id = $product_id GROUP BY ep.product_id ";
            $product = $db->fetchRow($query);
        } catch (Zend_Exception $e) {
            echo "Caught exception: " . get_class($e) . "\n";
            echo "Message: " . $e->getMessage() . "\n";
        }

        return $product;
    }

    public function fetchProductByAlias($alias) {
        $db = Zend_Db_Table::getDefaultAdapter();
        $sql = $this->_productQuery;
        try {
            $sql .= "WHERE ec.eshop_id = " . APP_ID . " AND (ep.alias_$this->lang = '$alias') GROUP BY ep.product_id ";
            $product_data = $db->fetchRow($sql);
            return $product_data;
            /*if (!empty($product_data)) {
                return new Model_EshopProduct($product_data);
            } */   
        } catch (Zend_Exception $e) {
            echo "Caught exception " . __METHOD__ . ": " . get_class($e) . "\n <br/>";
            echo "\n <br/>Message: " . $e->getMessage() . "\n <br/>";
            echo "\n <br/>SQL: " . $sql . "\n <br/>";
        }
    }   

    public function fetchProductsForXML($category_id_array = null) {
        //Tato metoda získá všechny produkty a jejich varianty dané subkategorie
        $db = Zend_Db_Table::getDefaultAdapter();
        $sql = $this->_productQuery;
        if (isset($category_id_array)) {
            $sql .= "WHERE (c.category_id IN (" . implode(", ", $category_id_array) . ")) AND (status = '1') ";
        }
        $sql .= "GROUP BY p.product_ID "
                . "ORDER BY sequence, title ";
        try {
            $data = $db->fetchAll($sql);
            return $data;
        } catch (Zend_Exception $e) {
            echo "Caught exception " . __METHOD__ . ": " . get_class($e) . "\n <br/>";
            echo "\n <br/>Message: " . $e->getMessage() . "\n <br/>";
            echo "\n <br/>SQL: " . $sql . "\n <br/>";
        }
    }

    public function fetchProductsSearch($search) {
        $db = Zend_Db_Table::getDefaultAdapter();
        $products = array();
        //$keyword = "%".preg_replace(' ', '[%]', $search)."%";
        $sql = $this->_productQuery . "WHERE c.eshop_id = " . APP_ID . " AND LOWER (p.title_cz) LIKE LOWER('%$search%') AND (status != '0') AND (status != '3') GROUP BY p.product_id LIMIT 30";
        //pošlu query
        try {
            $result = $db->fetchAll($sql);
            foreach ($result as $data) {
                $products[$data['product_id']] = new Model_EshopProduct($data);
            }
        } catch (Zend_Exception $e) {
            echo "Caught exception " . __METHOD__ . ": " . get_class($e) . "\n <br/>";
            echo "\n <br/>Message: " . $e->getMessage() . "\n <br/>";
            echo "\n <br/>SQL: " . $sql . "\n <br/>";
        }
        return $products;
    }
    
    public function fetchProductsByIds($idArray) {
        $query = $this->_productQuery . "WHERE ep.product_id IN (";
        $i = 0;
        foreach ($idArray as $value) {
            if ($i > 0) {$query .= ", ";}
            $query .= "$value";
            $i++;
        }
        $query .= ")";
        try {
            $db = Zend_Db_Table::getDefaultAdapter();
            $result = $db->fetchAll($query);
        } catch (Zend_Exception $e) {
            echo "<br/>Caught exception: " . get_class($e) . "\n";
            echo "<br/>Message: " . $e->getMessage() . "\n";
            echo "<br/>SQL: " . $query;
        }
        return $result;
    }

    public function fetchProductsByCat($category_id = null, $order = null, $activeOnly = true) {
        //$defaultSession = new Zend_Session_Namespace('Default');        
        //připravím
        if (!empty($category_id)) {
            if ($category_id == 'unsorted') {
                $query = $this->_productQuery . "WHERE ec.category_id IS NULL ";
                if ($activeOnly) { $query .= "AND ep.status_$this->locale > 0 "; }
            } else if ($category_id == 'all') {
                if ($activeOnly) { $query .= "WHERE ep.status_$this->locale > 0 "; }
                $query = $this->_productQuery_grouped . "GROUP BY ep.product_id ";
            } else {
                $query = $this->_productQuery_grouped . "WHERE ec.category_id = $category_id ";
                if ($activeOnly) { $query .= "AND ep.status_$this->locale > 0 "; }
                $query .= "GROUP BY ep.product_id ";
            }
        } else {
            if ($activeOnly) { $query .= "WHERE ep.status_$this->locale > 0 "; }
            $query = $this->_productQuery_grouped . "GROUP BY ep.product_id ";
        }
        //zjistím podle čeho to seřadit
        if (!empty($order)) {
            $query .= "ORDER BY $order";
        }
        //pošlu query
        try {
            $db = Zend_Db_Table::getDefaultAdapter();
            $result = $db->fetchAll($query);
        } catch (Zend_Exception $e) {
            echo "<br/>Caught exception: " . get_class($e) . "\n";
            echo "<br/>Message: " . $e->getMessage() . "\n";
            echo "<br/>SQL: " . $query;
        }
        return $result;
    }

    public function fetchProductsBySubcat($subcategory_id = null, $category_id = null, $activeOnly = true) {
        if (isset($subcategory_id)) {
            $db = Zend_Db_Table::getDefaultAdapter();
            $query = $this->_productQuery . "WHERE esp.subcategory_id = $subcategory_id ";
            if (isset($category_id)) {
                $query .= "AND ec.category_id = $category_id ";
            }
            if ($activeOnly) {
                $query .= "AND ep.status_$this->locale > 0 ";
            }
            //$query = "SELECT * FROM eshop_categories AS ec LEFT JOIN (eshop_subcategories AS es, eshop_subcat_products AS esp, eshop_products AS ep) ON (ec.category_id = es.category_id AND es.subcategory_id = esp.subcategory_id AND esp.product_id = ep.product_id) WHERE es.subcategory_id = $subcategory_id";
            $result = $db->fetchAll($query);
        }
        return $result;
    }

    public function fetchProductsByGroup($group_id = null, $limit = null, $page = null, $order = null) {
        $defaultSession = new Zend_Session_Namespace('Default');
        $defaultSession->lang;
        if (!empty($group_id)) {
            $db = Zend_Db_Table::getDefaultAdapter();
            $query = "SELECT     
                        eg.group_id, 
                        eg.title_cz AS group_title,
                        esg.subcategory_id,
                        ep.product_id, 
                        ep.status_$this->locale AS status,
                        ep.vat_rate,
                        es.subcategory_id,
                        es.title_cz AS subcategory_title,
                        es.type,
                        ep.title_cz AS title,
                        ep.text_cz AS text,
                        ep.producer,
                        ep.size,
                        ep.price_$this->locale AS price
                        FROM eshop_groups AS eg
                        JOIN (eshop_subcat_groups AS esg, eshop_subcategories AS es, eshop_products AS ep, eshop_subcat_products AS esp) 
                            ON (eg.group_id = esg.group_id AND esg.subcategory_id = es.subcategory_id AND esp.product_id = ep.product_id AND es.subcategory_id = esp.subcategory_id) 
                        WHERE eg.group_id = '$group_id' ";
            if (isset($order)) {
                $query .= "ORDER BY '$order'";
            }
            $result = $db->fetchAll($query);
        }
        return $result;
    }

    public function fetchProductsOfSubcategories($aParams) {
        //Tato metoda získá všechny produkty a jejich varianty dané subkategorie
        $db = Zend_Db_Table::getDefaultAdapter();
        $products = array();
        $sql = $this->_productQuery;
        $sql .= "WHERE (c.eshop_id = " . APP_ID . ") AND (status != '0') AND (status != '3') ";
        if (!empty($aParams['amount_upper']) && !empty($aParams['amount_lower'])) {
            $sql .= "AND p.price_" . APP_LOCALE . " BETWEEN '" . $aParams['amount_lower'] . "' AND '" . $aParams['amount_upper'] . "' ";
        }
        if (empty($aParams)) {
            //nic
        } else {
            foreach ($aParams['subcatIds'] as $subcategory_id) {
                if ($subcategory_id != 'default') {
                    $sql .= "AND p.product_id IN "
                        . "(SELECT esp.product_id FROM eshop_subcat_products AS esp WHERE esp.subcategory_id = '$subcategory_id') ";
                }
            }
        }
        $sql .= "GROUP BY p.product_ID ORDER BY sequence, title ";
        try {
            $result = $db->fetchAll($sql);
            foreach ($result as $data) {
                $products[$data['product_id']] = new Model_EshopProduct($data);
            }
        } catch (Zend_Exception $e) {
            echo "Caught exception " . __METHOD__ . ": " . get_class($e) . "\n <br/>";
            echo "\n <br/>Message: " . $e->getMessage() . "\n <br/>";
            echo "\n <br/>SQL: " . $sql . "\n <br/>";
        }
        return $products;
    }

    public function saveProduct($data, $subcats, $product_id = null) {
        $relationmodel = new Model_DbTable_SubcatProducts();
        $subcatmodel = new Model_DbTable_SubCategories();
        if (isset($product_id)) {
            $where = $this->getAdapter()->quoteInto('product_id = ?', $product_id);
            $this->update($data, $where);
            //vezmeme celý seznam subkategorií
            $subcategories = $subcatmodel->fetchAll()->toArray();
            //zjistíme, které subkategorie byli již k výrobku přiřazeny
            $prevselected = $relationmodel->fetchProductSubcats($product_id);
            foreach ($subcategories as $value) {
                if ((!in_array($value['subcategory_id'], $prevselected)) && (in_array($value['subcategory_id'], $subcats))) {
                    //subkategorie z postu není na seznamu z databáze, musí se tam přidat
                    $relationmodel->insert(array('subcategory_id' => $value['subcategory_id'], 'product_id' => $product_id));
                } else if ((in_array($value['subcategory_id'], $prevselected)) && (!in_array($value['subcategory_id'], $subcats))) {
                    $where = array();
                    $where[] = $relationmodel->getAdapter()->quoteInto('subcategory_id = ?', $value['subcategory_id']);
                    $where[] = $relationmodel->getAdapter()->quoteInto('product_id = ?', $product_id);
                    $relationmodel->delete($where);
                }
            }
        } else {
            $this->insert($data);
            $db = Zend_Db_Table::getDefaultAdapter();
            $product_id = $db->lastInsertId($this->_name, $this->_primary);
            foreach ($subcats as $value) {
                $relationmodel->insert(array('subcategory_id' => $value, 'product_id' => $product_id));
            }
        }
        return $product_id;
    }

    public function fetchProductSales($category_id = null, $date_from = null, $date_to = null, $year = null, $order = null) {
        $db = Zend_Db_Table::getDefaultAdapter();
        $sql = "SELECT t1.order_detail_id, t1.product_id, t1.status_cz AS status, t1.title_cz AS title, t1.producer, t1.price_cz AS price, t1.vat_rate,"
                . "SUM(t1.product_price) AS product_price, SUM(t1.product_quantity) AS product_quantity "
                . "FROM ("
                . "SELECT DISTINCT eod.order_detail_id, ep.product_id, ep.status_cz, ep.title_cz, ep.alias_cz, ep.producer, ep.size, ep.price_cz, ep.vat_rate,"
                . "eod.product_price, eod.product_quantity "
                . "FROM eshop_products AS ep "
                . "LEFT JOIN eshop_order_details AS eod ON ep.product_id = eod.product_id "
                . "JOIN eshop_orders AS eo ON eo.order_id = eod.order_id "
                . "LEFT JOIN (eshop_subcat_products AS esp, eshop_subcategories AS es, eshop_categories AS ec) 
                            ON (esp.product_id = ep.product_id AND es.subcategory_id = esp.subcategory_id AND ec.category_id = es.category_id) "
                . "WHERE eo.status_id != 0 ";
        if (isset($category_id) and $category_id != 'all') {
            $sql .= "AND ec.category_id = $category_id ";
        }
        if (isset($year) && $year != 'all') {
            $sql .= "AND eo.order_timestamp >= '$year-01-01 00:00:00' AND eo.order_timestamp <= '$year-12-31 23:59:59' ";
        }
        if (isset($date_from) && isset($date_to)) {
            $sql .= "AND eo.order_timestamp >= '$date_from 00:00:00' AND eo.order_timestamp <= '$date_to 23:59:59' ";
        }
        $sql .= ")AS t1 GROUP BY t1.product_id ";
        if (isset($order)) {
            $sql .= "ORDER BY $order ";
            if ($order == 'product_price' or $order == 'product_quantity') {
                $sql .= "DESC ";
            }
        } else {
            $sql .= "ORDER BY product_price DESC ";
        }
        try {
            $result = $db->fetchAll($sql);
        } catch (Zend_Exception $e) {
            echo "<p>SQL: $sql </p>";
            echo "<p>Caught exception during saving order details: " . get_class($e) . "</p>";
            echo "<p>Message: " . $e->getMessage() . "</p>";
        }
        return $result;
    }

}
