<?php

class Admin_KontaktController extends Zend_Controller_Action
{

    public function init() {
        $this->view->headTitle('Kontaktní údaje', 'POSTEND');
    }

    public function indexAction() {
        //vylovíme daný text v mnohojazyčné podobě
        $model = new Model_DbTable_Texts();
        $text  = $model->find('kontakt')->toArray();
        $form  = new Form_TextformCZEN('/admin/pivo/save/');        
        //nasolíme text do připraveného formuláře (pro češtinu a angličtinu)
        $form->populate($text[0]);
        $this->view->form = $form;
    }

    public function saveAction() {
        $table = new Model_DbTable_Texts();
        $table->updateText();
        $this->_redirect('/admin/kontakt/');
    }

}

