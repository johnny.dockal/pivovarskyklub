<?php
// Define path to application directory
defined('APPLICATION_PATH') || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../application'));

//z nějakého neznámého důvodu mi nejde na ONEBITU nastavit v .htaccess APPLICATION_ENV na cokoliv jiného než production
//takže takhle to ochcávám, aby se mi tam nahodilo "testing" v případě, že to běží na demu
$url = $_SERVER['HTTP_HOST'];
$url_array = explode('.', $url);
if (isset($url_array[0]) and ($url_array[0] == 'test')) {
    $enviroment = 'testing';
} else if ($url_array[1] == 'local') {
    $enviroment = 'development';
} else {
    $enviroment = 'production';
}

// Define application environment
defined('APPLICATION_ENV') || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : $enviroment));

// Define application name
defined('APP_NAME') || define('APP_NAME', 'pivoklub');

// Define application name
defined('APP_NAME_FULL') || define('APP_NAME_FULL', 'Restaurace Pivovarský Klub');

// Define application ID 1=pivovarský klub
defined('APP_ID') || define('APP_ID', 1);

// Define application locale
defined('APP_LOCALE') || define('APP_LOCALE', 'cz');

// Define application email
define('APPLICATION_EMAIL', 'info@pivovarskyklub.com');

// Define application contact address for testing purposes
defined('APP_TESTMAIL') || define('APP_TESTMAIL', 'jan_dockal@seznam.cz');

// Define product url (in case images are taken from external source)
defined('APP_URL') || define('APP_URL', 'http://www.pivovarskyklub.com/');

// Define product url (in case images are taken from external source)
defined('APP_URL_TEST') || define('APP_URL_TEST', 'http://test.pivovarskyklub.com/');

// Define product url (in case images are taken from external source)
defined('IMG_URL') || define('IMG_URL', 'http://www.pivovarskyklub.com/');

// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
    realpath(APPLICATION_PATH . '/../library'),
    get_include_path(),
)));

/** Zend_Application */
require_once 'Zend/Application.php';

// Create application, bootstrap, and run
$application = new Zend_Application(
        APPLICATION_ENV, APPLICATION_PATH . '/configs/application.ini'
);

//Setup Ini Configuration, save to registry
$config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', 'staging');
Zend_Registry::set('config', $config);

$front = Zend_Controller_Front::getInstance();
//Setup Router - SEO URL
$router = $front->getRouter();
//stálý jídelní lístek
$listek = new Zend_Controller_Router_Route(
        'staly-jidelni-listek', 
        array(
            'module' => 'default',
            'controller' => 'menu',
            'action' => 'index',
            'view' => 'listek'
        )
);
$router->addRoute('staly-jidelni-listek', $listek);
//polední nabídka
$obedy = new Zend_Controller_Router_Route(
        'poledni-nabidka', 
        array(
            'module' => 'default',
            'controller' => 'menu',
            'action' => 'index',
            'view' => 'nabidka'
        )
);
$router->addRoute('poledni-nabidka', $obedy);
//polední nabídka
$nabidka = new Zend_Controller_Router_Route(
        'nabidka-dne', 
        array(
            'module' => 'default',
            'controller' => 'menu',
            'action' => 'index',
            'view' => 'obedy'
        )
);
$router->addRoute('nabidka-dne', $nabidka);
//nabídka menu
$menu = new Zend_Controller_Router_Route(
        'nabidka-menu', 
        array(
            'module' => 'default',
            'controller' => 'menu',
            'action' => 'index',
            'view' => 'menu'
        )
);
$router->addRoute('nabidka-menu', $menu);

$application->bootstrap()
            ->run();

function dump($var) {
    echo "<pre>";
    var_dump($var);
    echo "</pre>";
}

function sanitizeInputInt($i) {
    $temp = str_replace(',', '.', $i);
    $temp2 = preg_replace('/[^0-9.]*/', '', $temp);
    return ceil($temp2);
}