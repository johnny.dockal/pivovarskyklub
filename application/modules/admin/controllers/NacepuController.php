<?php

class Admin_NacepuController extends Zend_Controller_Action
{

    public function init() {
        $this->view->headTitle('Piva na čepu', 'POSTEND');
    }

    public function indexAction() {
        //vylovíme daný text v mnohojazyčné podobě
        $modelTexts = new Model_DbTable_Texts();
        $text       = $modelTexts->fetchBeer();
        //nasolíme text do připraveného formuláře (pro češtinu a angličtinu)
        $form       = new Form_Pivoform('/admin/nacepu/save/', $text);
        $this->view->form = $form;
        $this->view->text = $text;
    }

    public function saveAction() {
        $table = new Model_DbTable_Texts();
        $data = array(
            'text_cz'      => $this->getParam('text_nacepu_cz'),
            'text_en'      => $this->getParam('text_nacepu_en')
        );
        $where = $table->getAdapter()->quoteInto('text_id = ?', 'nacepu');
        $table->update($data, $where);
        $data = array(
            'title_cz'     => $this->getParam('title_cz_pivo1'),
            'text_cz'      => $this->getParam('text_cz_pivo1'),
            'title_en'     => $this->getParam('title_cz_pivo1'),
            'text_en'      => $this->getParam('text_cz_pivo1')
        );
        $where = $table->getAdapter()->quoteInto('text_id = ?', 'pivo1');
        $table->update($data, $where);
        $data = array(
            'title_cz'     => $this->getParam('title_cz_pivo2'),
            'text_cz'      => $this->getParam('text_cz_pivo2'),
            'title_en'     => $this->getParam('title_cz_pivo2'),
            'text_en'      => $this->getParam('text_cz_pivo2')
        );
        $where = $table->getAdapter()->quoteInto('text_id = ?', 'pivo2');
        $table->update($data, $where);
        $data = array(
            'title_cz'     => $this->getParam('title_cz_pivo3'),
            'text_cz'      => $this->getParam('text_cz_pivo3'),
            'title_en'     => $this->getParam('title_cz_pivo3'),
            'text_en'      => $this->getParam('text_cz_pivo3')
        );
        $where = $table->getAdapter()->quoteInto('text_id = ?', 'pivo3');
        $table->update($data, $where);
        $data = array(
            'title_cz'     => $this->getParam('title_cz_pivo4'),
            'text_cz'      => $this->getParam('text_cz_pivo4'),
            'title_en'     => $this->getParam('title_cz_pivo4'),
            'text_en'      => $this->getParam('text_cz_pivo4')
        );
        $where = $table->getAdapter()->quoteInto('text_id = ?', 'pivo4');
        $table->update($data, $where);
        $data = array(
            'title_cz'     => $this->getParam('title_cz_pivo5'),
            'text_cz'      => $this->getParam('text_cz_pivo5'),
            'title_en'     => $this->getParam('title_cz_pivo5'),
            'text_en'      => $this->getParam('text_cz_pivo5')
        );
        $where = $table->getAdapter()->quoteInto('text_id = ?', 'pivo5');
        $table->update($data, $where);
        $data = array(
            'title_cz'     => $this->getParam('title_cz_pivo6'),
            'text_cz'      => $this->getParam('text_cz_pivo6'),
            'title_en'     => $this->getParam('title_cz_pivo6'),
            'text_en'      => $this->getParam('text_cz_pivo6')
        );
        $where = $table->getAdapter()->quoteInto('text_id = ?', 'pivo6');
        $table->update($data, $where);
        $this->_redirect('/admin/nacepu/');
        
    }

}

