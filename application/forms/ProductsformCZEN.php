<?php

/**
 * Formulář k přihlášení se do administrace.
 *
 * @package default
 * @author Daniel Vála
 */
class Form_ProductsformCZEN extends Zend_Form {

    public function __construct($action = null, $options = null, $selected = null) {
        parent::__construct();
        $this->setMethod('POST')->setName('login')->setAction($action);
        $this->setAttrib('enctype', 'multipart/form-data');
        
        //momentálně je formulář nastaven na dva jazyky (cz a en), nevím jak ho upravit pro libovolný počet jazyků z configu
        $product_id = new Zend_Form_Element_Hidden('product_id', array('class' => 'nodisplay', 'value' => ''));
        $product_id->removeDecorator('Label');
        $category1 = new Zend_Form_Element_Submit('pivo', array('label' => "Pivo", 'class' => "button bg-green border-style active"));
        $category1->removeDecorator('DtDdWrapper');
        $category2 = new Zend_Form_Element_Submit('predmety', array('label' => "Sběratelské předměty", 'class' => "button bg-green border-style active")); 
        $category2->removeDecorator('DtDdWrapper');
        $category3 = new Zend_Form_Element_Submit('homebrew', array('label' => "Homebrew", 'class' => "button bg-green border-style active"));
        $category3->removeDecorator('DtDdWrapper');
        
        $subcategory_id = new Zend_Form_Element_MultiCheckbox('subcategory_id');
        $subcategory_id->setLabel('Subkategorie:')->setRequired(true);
        if (isset($options)) {
            foreach ($options as $value) {
               $subcategory_id->addMultiOption($value['subcategory_id'], $value['title']);
            }
            $subcategory_id->setValue($selected);
        }
        
        $status_cz = new Zend_Form_Element_Select('status_cz', array('class' => 'textboxwide'));
        $status_cz->addMultiOptions(array('1' => 'prodavan', '2' => 'vystaven','0' => 'neaktivni'));
        $status_cz->setLabel('Status výrobku:')->setRequired(true);
        
        $vat_rate = new Zend_Form_Element_Radio('vat_rate');
        $vat_rate->addMultiOptions(array('1' => 'standartní', '2' => 'snížená'));
        $vat_rate->setLabel('Sazba DPH:')->setRequired(true);
                
        $title_cz = new Zend_Dojo_Form_Element_TextBox('title_cz', array('class' => 'textboxwide', 'value' => ''));
        $title_cz->setLabel('Nadpis česky:')->setRequired(true);

        $text_cz = new Zend_Dojo_Form_Element_Textarea('text_cz', array('class' => "textboxbig", 'value' => ''));
        $text_cz->setLabel('Text česky:')->setRequired(true);
        
        $title_en = new Zend_Dojo_Form_Element_TextBox('title_en', array('class' => 'textboxwide', 'value' => ''));
        $title_en->setLabel('Nadpis anglicky:')->setRequired(true);

        $text_en = new Zend_Dojo_Form_Element_Textarea('text_en', array('class' => "textboxbig", 'value' => ''));
        $text_en->setLabel('Text anglicky:')->setRequired(true);
               
        $producer = new Zend_Dojo_Form_Element_TextBox('producer', array('class' => 'textboxwide', 'value' => ''));
        $producer->setLabel('Pivovar:')->setRequired(true);
       
        $size = new Zend_Dojo_Form_Element_TextBox('size', array('class' => 'textboxwide', 'value' => ''));
        $size->setLabel('Velikost/objem:')->setRequired(true);
        
        $price_cz = new Zend_Dojo_Form_Element_TextBox('price_cz', array('class' => 'textboxwide', 'value' => ''));
        $price_cz->setLabel('Cena koruny:')->setRequired(true);
        
        $image = new Zend_Form_Element_File('product_image');
        $image->setLabel('Obrázek produktu formátu 1*2 (láhve a sklenice):');
        $image->addValidator('Count', false, 1);
        //$image->addValidator('Size', false, 10240000);
        $image->addValidator('Extension', false, 'jpg,JPG');
        
        $image2 = new Zend_Form_Element_File('product_image2');
        $image2->setLabel('Obrázek produktu 2*3 (slady atd.):');
        $image2->addValidator('Count', false, 1);
        //$image->addValidator('Size', false, 10240000);
        $image2->addValidator('Extension', false, 'jpg,JPG');
        
        $save = new Zend_Form_Element_Submit('save', array('label' => "Uložit", 'class' => "button bg-green border-style active", 'style' => 'width: 99%;'));

        $this->addElements(array(
            $product_id, 
            $category1, 
            $category2, 
            $category3, 
            $subcategory_id, 
            $status_cz, 
            $vat_rate,
            $title_cz, 
            $text_cz, 
            $title_en, 
            $text_en, 
            $producer, 
            $size, 
            $price_cz, 
            $image,
            $image2,
            $save
        ));
    }
}
