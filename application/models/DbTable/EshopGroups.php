<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class Model_DbTable_EshopGroups extends Zend_Db_Table_Abstract {

    protected $_name = 'eshop_groups';
    protected $_primary = 'group_id';
    
    function fetchGroupedSubcategories($category_id) {
        $session = new Zend_Session_Namespace('Default');
        $db             = Zend_Db_Table::getDefaultAdapter();
        $query =    "SELECT eg.group_id, eg.category_id, es.subcategory_id, eg.title_$session->lang AS group_title, es.title_$session->lang AS subcat_title "
                    . "FROM $this->_name AS eg "
                    . "JOIN eshop_subcat_groups AS esg ON eg.group_id = esg.group_id "
                    . "JOIN eshop_subcategories AS es ON esg.subcategory_id = es.subcategory_id "
                    . "WHERE eg.category_id = '$category_id' "
                    . "ORDER BY eg.group_id, es.title_$session->lang";
        $result         = $db->fetchAll($query);
        return $result;
    }
    
}