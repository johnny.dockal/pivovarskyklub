<?php

class Admin_TextsController extends Zend_Controller_Action {

    public function init() {
        $this->view->headTitle('Články - Klubové akce', 'POSTEND');
    }

    public function indexAction() {
        $model = new Model_DbTable_EshopTexts();
        
        $this->view->texts = $model->fetchAll();
    }

    public function editAction() {          
        $text_id = $this->_getParam('text_id');        
        $model = new Model_DbTable_EshopTexts();
        $text       = $model->find($text_id)->toArray();
        //nasolíme text do připraveného univerzálního formuláře
        $form       = new Form_Textform($text[0]);
        $this->view->form = $form;
    }

    public function saveAction() {
        $table = new Model_DbTable_EshopTexts();
        $table->updateText();        
        $this->_redirect('/admin/texts/');
    }
}

