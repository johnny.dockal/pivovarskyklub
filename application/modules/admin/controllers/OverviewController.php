<?php

class Admin_OverviewController extends Zend_Controller_Action {

    private $category_id = 'all';
    private $page = 1;
    private $order = 'product_id';
    private $sort = 'desc';
    private $limit = 50;

    public function init() {
        /* Initialize action controller here */
    }
    public function preDispatch() {
        $category_id = $this->getParam('category_id');
        $page = $this->getParam('page');
        $order = $this->getParam('order');
        $sort = $this->getParam('sort');
        //$year = $this->getParam('year');
        $date_from = $this->getParam('date_from');
        $date_to = $this->getParam('date_to');
        
        $adminSession = new Zend_Session_Namespace('Admin');
        
        if (!empty($category_id)) {
            $this->category_id = $category_id;
            $adminSession->category_id = $category_id;
            //pokus se mění kategorie, musí se jít zpátky na stránku 1, aby nedošlo k out of bounds exception
            $adminSession->page = $this->page;
        } else {
            $this->category_id = $adminSession->category_id;
        }
        if (empty($adminSession->category_id)) {
            $adminSession->category_id = $this->category_id;
        }
        //nastavíme stránku
        if (!empty($page)) {
            $this->page = $page;
            $adminSession->page = $page;
        } else if (empty($adminSession->page)) {            
            $adminSession->page = $this->page;
        } else {
            $this->page = $adminSession->page;
        }  
        //nastavíme pořadí
        if (!empty($order)) {
            $this->order = $order;
            $adminSession->order = $order;
        } else {
            $this->order = $adminSession->order;
        }
        if (empty($adminSession->order)) {
            $adminSession->order = $this->order;
        }
        if (!empty($sort)) {
            $this->sort = $sort;
            $adminSession->sort = $sort;
        } else {
            $this->sort = $adminSession->sort;
        }
        /*
        if (!empty($year)) {
            $this->year = $year;
            $adminSession->year = $this->year;
        } else {
            $this->year = $adminSession->year;
        }
        if (empty($adminSession->sort)) {
            $adminSession->sort = $this->sort;
        }*/
        if (!empty($date_from)) {
            $this->date_from = $date_from;
            $adminSession->date_from = $this->date_from;
        } else {
            $this->date_from = $adminSession->date_from;
        }
        if (!empty($date_to)) {
            $this->date_to = $date_to;
            $adminSession->date_to = $this->date_to;
        } else {
            $this->date_to = $adminSession->date_to;
        }
        if (empty($adminSession->sort)) {
            $adminSession->sort = $this->sort;
        }
        
        $this->view->category_id = $adminSession->category_id;
        $this->view->page = $adminSession->page;
        $this->view->order = $adminSession->order;
        //$this->view->year = $adminSession->year;
        $this->view->date_from = $adminSession->date_from;
        $this->view->date_to = $adminSession->date_to;
        $this->view->sort = $adminSession->sort;
    }

    public function indexAction() {
        $categories = new Model_DbTable_EshopCategories();
        $model = new Model_DbTable_EshopProducts();    
        
        $products = $model->fetchProductSales($this->category_id, $this->date_from, $this->date_to, null, $this->order);  
        $this->view->categories = $categories->fetchAll()->toArray();
        $this->view->itemlimit = $this->limit;
        $this->view->itemcount = count($products);
        $this->view->pagecount = ceil($this->view->itemcount / $this->limit);        
        $this->view->products = array_chunk($products, $this->limit);
    }   
    
    public function detailAction() {        
        $product_id = $this->getParam('product_id');
        $modelProduct = new Model_DbTable_EshopProducts();
        $product = $modelProduct->fetchProduct($product_id);
        $this->view->productName = $product['title'];
        $status = new Model_DbTable_EshopOrderStatus();
        $statusArray = $status->fetchAll('status_id >= 0')->toArray();
        $this->view->status = $statusArray;
        $model = new Model_DbTable_EshopOrders();
        $this->view->orders = $model->fetchOrdersByProduct($product_id, $this->date_from, $this->date_to);
    }

}
