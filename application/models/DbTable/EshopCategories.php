<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class Model_DbTable_EshopCategories extends Zend_Db_Table_Abstract {

    protected $_name = 'eshop_categories';
    protected $_primary = 'category_id';
    
    public function fetchCategories() {
        $defaultSession = new Zend_Session_Namespace('Default');
        $query = $this->select()->from($this->_name, array('category_id', 'sequence','title_'.$defaultSession->lang.' AS title', 'text_'.$defaultSession->lang.' AS text'));
        $result = $this->fetchAll($query)->toArray();
        return $result;
    }

}