<?php
class Admin_JidloController extends Zend_Controller_Action {
        
    private $text           = 'jidelak';
    private $text_backup    = 'jidelak-zaloha';

    public function init() {
        $this->view->headTitle('Jídla', 'POSTEND');
    }

    public function indexAction() {
        $model = new Model_DbTable_Texts;
        $text = $model->find($this->text)->toArray();
        $form = new Form_Textform($text[0], '/admin/jidlo/save/');
        $this->view->form = $form;
        $this->view->backup_date = $model->fetchLastUpdate($this->text_backup);
    }
    
    public function backupAction() {
        $model = new Model_DbTable_Texts;
        $text = $model->find($this->text_backup)->toArray();
        $form = new Form_Textform($text[0], '/admin/jidlo/save/');
        $this->view->form = $form;
        $this->view->backup_date = $model->fetchLastUpdate($this->text_backup);
        $this->render('index');
    }

    public function saveAction() {
        $table = new Model_DbTable_Texts();
        if ($this->getRequest()->getPost('backup') !== NULL) {
            $table->updateBackup($this->text_backup);   
        } else {
            $table->updateText();            
        }        
        $this->_redirect('/admin/jidlo/');
    }
}

