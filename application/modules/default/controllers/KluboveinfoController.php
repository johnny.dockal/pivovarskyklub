<?php

class Default_KluboveinfoController extends Zend_Controller_Action
{
    private $action;
    
    public function init()
    {
        $this->action = $this->getRequest()->getActionName();
        $this->view->action = $this->action;
    }

    public function indexAction() //kategorie "klubové akce", category_id = 2
    {
        $articleModel = new Model_DbTable_Articles();
        $articles = $articleModel->fetchAll("status = 'public' AND category_id = '2'");
        
        $this->view->articles = $articles;
    }

    public function pravidlaAction()
    {
    }
    
    public function clankyAction() //kategorie "jiné články", category_id = 3
    {
        $articleModel = new Model_DbTable_Articles();
        $articles = $articleModel->fetchAll("status = 'public' AND category_id = '3'");
        
        $this->view->articles = $articles;       
    }
    
    public function clanekAction()
    {
        $article_id   = $this->_request->getParam('article_id'); 
        $articleModel = new Model_DbTable_Articles();     
        $this->view->article = $articleModel->find($article_id);        
    }
}

