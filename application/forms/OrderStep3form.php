<?php
/**
 * Synchronizováno 2015-12-05
 */
class Form_OrderStep3form extends Zend_Form { 

    public function __construct($paymentOptions, $deliveryCount = 1, $totalPrice = null) {
        parent::__construct();    
        //načteme view, ve kterém jsou již připravené texty v patřičném jazyce        
        $view = Zend_Layout::getMvcInstance()->getView();  
        if ($view->project == 'pivoklub') {
            $this->setMethod('POST')->setName('login')->setAction('/eshop/cart/order/?step=4');
        } else {
            $this->setMethod('POST')->setName('login')->setAction('/cart/order/?step=4');
        }        
        $payment_options_formatted = array();
        $this->setAttrib('class', 'orderform');
        foreach ($paymentOptions as $value) {
            $note = "";
            //ochcávka kvůli cenám v německu, teď se to musí převést do eura z korun
            $session   = new Zend_Session_Namespace('Default');
            if ($session->locale == 'de') {
                $price = $view->currency->exchangeToEuroNumber($value['price']);
            } else {
                $price = $value['price'];
            }
            //zde si spočítáme, kolik ta cena vlastně je
            //mohou to být procenta
            if (!empty($value['percentage'])) { 
                /* Musíme zjistit, zda se mají počítat procenta nebo fixní částka.
                 * K tomu je třeba zjistit hodnotu payment_break_point, která výpočet dělí na procentuální nebo fixní.
                */
                $order = new Model_EshopOrder();
                if ($totalPrice > $order->payment_break_point) {
                    $percent = ($totalPrice / 100) * $value['percentage'];
                    $percent = round($percent, 0, PHP_ROUND_HALF_UP);                    
                    $priceString = "+".$value['percentage']."%";
                    $priceString .= " $view->str_order_percent_of ". $view->currency->toCurrency($totalPrice)." = ".$view->currency->toCurrency($percent);
                } else {
                    $priceString = "+".$view->currency->toCurrency($price);
                }
            //může se to zaúčtovat vícekrát, pokud je objednávka rozdělena do více balíčků
            } else if (($deliveryCount > 1) && ($price > 0)) {
                $sum = $deliveryCount * $price;
                $priceString = $deliveryCount . "x " . $price . " = +" . $sum;
                $note = "<p class='order-description'>*Vaše objednávka bude rozdělena do $deliveryCount balíčků.</p>"; 
            } else {
                $priceString = "+".$view->currency->toCurrency($price);
            }
            if ($value['public'] == 1) {
                $payment_options_formatted[$value['payment_id']] = "&nbsp;" . $value['title'] . " <p class='order-description'>" . $value['text'] . "</p><p class='order-description'>$view->str_price0 <strong> " . $priceString . "</strong></p>" . $note;
            }
        }
        $payment = new Zend_Form_Element_Radio('payment_id', array(
                    'multiOptions' => $payment_options_formatted,
                    'escape' => false,
                    'required' => true
                ));
        $payment->setLabel($view->str_order_select_payment); 
        
        $message = new Zend_Form_Element_Textarea('order_message', array('style' => 'height: 100px; width: 98%'));
        $message->setAttrib('maxlength','1000');  
        $message->setLabel($view->str_order_message);   
        
        $submit = new Zend_Form_Element_Submit('submit', array('label' => $view->str_order_continue, 'class' => "button active cufon"));

        $this->addElements(array($payment, $message, $submit));
    }

}
