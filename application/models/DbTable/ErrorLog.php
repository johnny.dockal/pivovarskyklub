<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class Model_DbTable_ErrorLog extends Zend_Db_Table_Abstract {

    private static $_instance = null; // use private here
    protected $_name = 'error_log';
    protected $_primary = 'id';
    
    public static function getInstance()
    {
        if (self::$_instance === null) { // use strictly equal to null
            self::$_instance = new self();
        }

        return self::$_instance;
    }
    
    public function log($url) {
        $data['url'] = $url;
        $this->insert($data);
    }    
}