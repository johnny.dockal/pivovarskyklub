<?php

/**
 * Formulář k přihlášení se do administrace.
 *
 * @package default
 * @author Daniel Vála
 */
class Form_Textform extends Zend_Form {

    public function __construct($text, $action = '/admin/texts/save/') {
        parent::__construct();
        $this->setMethod('POST')->setName('login')->setAction($action);
        $this->setAttrib('class', 'admintable');
       
        $text_id = new Zend_Form_Element_Hidden('text_id');
        //zabrání zobrazení labelu
        $text_id->setDecorators(array('ViewHelper'));
        $text_id->setValue($text['text_id']);
        $this->addElement($text_id);
        
        if (isset($text['title_cz'])) {
            $title_cz = new Zend_Dojo_Form_Element_TextBox('title_cz', array('class' => 'textboxwide'));
            $title_cz->setLabel('Nadpis česky:')->setRequired(true);
            $title_cz->setValue($text['title_cz']);
            $this->addElement($title_cz);  
        }    
        if (isset($text['text_cz'])) {        
            $text_cz = new Zend_Dojo_Form_Element_Textarea('text_cz', array('class' => "textboxhuge"));
            $text_cz->setLabel('Text česky:')->setRequired(true);
            $text_cz->setValue($text['text_cz']);
            $this->addElement($text_cz);
        }    
        if (isset($text['title_en'])) {
            $title_en = new Zend_Dojo_Form_Element_TextBox('title_en', array('class' => 'textboxwide'));
            $title_en->setLabel('Nadpis anglicky:')->setRequired(true);
            $title_en->setValue($text['title_en']);
            $this->addElement($title_en);  
        }    
        if (isset($text['text_en'])) {        
            $text_en = new Zend_Dojo_Form_Element_Textarea('text_en', array('class' => "textboxhuge"));
            $text_en->setLabel('Text anglicky:')->setRequired(true);
            $text_en->setValue($text['text_en']);
            $this->addElement($text_en);
        }    
        if (isset($text['title_de'])) {
            $title_de = new Zend_Dojo_Form_Element_TextBox('title_de', array('class' => 'textboxwide'));
            $title_de->setLabel('Nadpis německy:')->setRequired(true);
            $title_de->setValue($text['title_de']);
            $this->addElement($title_de);  
        }    
        if (isset($text['text_de'])) {        
            $text_de = new Zend_Dojo_Form_Element_Textarea('text_de', array('class' => "textboxhuge"));
            $text_de->setLabel('Text německy:')->setRequired(true);
            $text_de->setValue($text['text_de']);
            $this->addElement($text_de);
        }
        if (isset($text['title_ru'])) {
            $title_ru = new Zend_Dojo_Form_Element_TextBox('title_ru', array('class' => 'textboxwide'));
            $title_ru->setLabel('Nadpis rusky:')->setRequired(true);
            $title_ru->setValue($text['title_ru']);
            $this->addElement($title_ru);  
        }    
        if (isset($text['text_ru'])) {        
            $text_ru = new Zend_Dojo_Form_Element_Textarea('text_ru', array('class' => "textboxhuge"));
            $text_ru->setLabel('Text rusky:')->setRequired(true);
            $text_ru->setValue($text['text_ru']);
            $this->addElement($text_ru);
        }    

        $submit = new Zend_Form_Element_Submit('submit', array('label' => "Uložit", 'class' => "savebutton"));
        $this->addElement($submit);
        
        $backup = new Zend_Form_Element_Submit('backup', array('label' => "Uložit jako zálohu", 'class' => "savebutton", 'onclick' => "return confirm('Před uložením textu coby zálohy je nutné text zkontrolovat na stránkách!')"));
        $this->addElement($backup);
    }
}
