<?php

class Admin_UsersController extends Zend_Controller_Action
{

    public function init() {
        $this->view->headTitle('Uživatelé', 'POSTEND');
    }

    public function indexAction() {
        //připravíme formulář pro přidání nového uživatele
        $form = new Form_Userform();
        $this->view->form = $form;
        //připravíme výtah z tabulky uživatelů pro tabulku
        $modelUsers = new Model_DbTable_Users();
        $users = $modelUsers->fetchUsers();
        $this->view->users = $users;
    }
    
    public function saveAction() {
        $table = new Model_DbTable_Users();
        $data = array(
            'user_id'           => '', 
            'user_login'        => $this->getParam('user_login'),
            'user_email'        => $this->getParam('user_email'),
            'user_name'         => $this->getParam('user_name'),
            'user_role'         => $this->getParam('user_role'),
            'user_login_hashed' => md5($this->getParam('user_login')),
            'user_pswd_hashed'  => md5($this->getParam('user_pswd'))
        );
        $table->insert($data);
        $this->_redirect('/admin/users/');
    }
    
    public function deleteAction() {
        $user_id = $this->getParam('user_id');
        if (isset($user_id)) {
            $table = new Model_DbTable_Users();
            $table->deleteUser($user_id);
        } 
        $this->_redirect('/admin/users/');
    }


}

