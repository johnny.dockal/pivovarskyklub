<?php

class Model_CurrencyKoruna extends Zend_Currency {

    private $currency = null;
    
    function __construct() {
        $this->currency = new Zend_Currency('cs_CZ');
        $this->setFormat(array('currency' =>  'CZK', 'name' =>'koruna', 'symbol' => ' kč', 'precision' => 2, 'position' => Zend_Currency::RIGHT));       
    }
    
    public function toCurrency($input = null, array $options = Array()) {
        $output = $this->currency->toCurrency($input, $options); 
        return $output;
    }
}
