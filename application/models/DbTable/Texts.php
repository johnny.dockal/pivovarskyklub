<?php

/**
 * Model tabulky article.
 *
 * @package default
 * @author Daniel Vála
 */
class Model_DbTable_Texts extends Zend_Db_Table_Abstract {

    protected $_name = 'texts';
    protected $_primary = 'text_id';

    /**
     * Vybere z databáze všechny texty na stránky.
     * Předpokládá se, že text_id článku bude pojmenovávat typ článku.
     * Text_id se ve výsledném řetězci přehazuje jako klíč k poli $textarray, aby k tomu byl jednodužší přístup.
     * 
     * @return Zend_Db_Table_Rowset_Abstract
     */
    
    /* vrátí všechny texty v tabulce texts v daném jazyce 
     * 
     * @return array
     */
    public function fetchTexts() {
        $texts = $this->select();
        $session = new Zend_Session_Namespace('Default');
        $lang = $session->lang;
        //jazyk musí být dvoupísmenná zkratka, jestli tomu tak je se kontroluje už v pluginu LangSelector
        $texts->from('texts', array('last_update', 'text_id', 'title_'.$lang.' AS title', 'text_'.$lang.' AS text'));
        
        $result     = $this->fetchAll($texts);
        $textarray  = $result->toArray();
        for ($i = 0; $i < count($textarray); $i++) {
            $textarray[$i]['last_update']   = date('j. m. Y', strtotime($textarray[$i]['last_update']));
            $newkey                         = $textarray[$i]['text_id'];
            $textarray[$newkey]             = $textarray[$i];
            unset($textarray[$i]);
        }
        return $textarray;
    }
    
    public function fetchText($id, $lang = null) {
        $texts = $this->select();
        $session = new Zend_Session_Namespace('Default');
        if (!$lang) {
            $lang = $session->lang;
        }        
        //jazyk musí být dvoupísmenná zkratka, jestli tomu tak je se kontroluje už v pluginu LangSelector
        $texts->from('texts', array('last_update', 'text_id', 'title_'.$lang.' AS title', 'text_'.$lang.' AS text'));
        $texts->where("text_id = '$id'");
        $result     = $this->fetchRow($texts);
        if ($result) {
            $textarray  = $result->toArray();
            return $textarray;
        } else {
            return null;
        }
        
    }
    
    public function fetchLastUpdate($id) {
        $texts = $this->select();
        $texts->from('texts', array('last_update'));
        $texts->where("text_id = '$id'");
        $result     = $this->fetchRow($texts);
        if ($result) {
            $textarray  = $result->toArray();
            return $textarray['last_update'];
        } else {
            return null;
        }
    }
    
    /* vrátí jeden vybraný text z tabulky texts ve všech jazykových verzích (je potřeba zobecnit) */
    public function fetchBeer() {
        $texts = $this->select();
        //tohle je třeba zobecnit pro libovolně nastavený počet jazyků
        $texts->from($this->_name, array('text_id', 'title_cz', 'text_cz', 'text_en'));
        $beer = array('nacepu', 'pivo1', 'pivo2', 'pivo3', 'pivo4', 'pivo5', 'pivo6');
        $texts->where('text_id IN (?)', $beer);
        $result = $this->fetchAll($texts);
        return $result->toArray();
    }
    
    public function updateAkce($text) {
        $data = array(
            'text_cz'      => $text,
            'text_en'      => $text
        );
        $where = $this->getAdapter()->quoteInto("text_id = 'akce'");
        $this->update($data, $where);
    }
    
    /* Vzhledem k tomu, že formuláře standartních textů jsou všechny stejné, 
     * můžeme si dovolit ukládat texty zde, abychom nemuseli sbírat parametry v každém kontrolleru zvlášť 
     * POZOR, tohle je nestandartní, normálně by se měli parametry předávat z controlleru, ale
     * tohle mi zabrání duplikaci kódu v kontrolerech. 
     * zároveň si tato funkce přečte, kolik je jazyků v configu, takže je použitelná všeobecně
     * pro libovolný počet jazyků v nastavení
     */
    public function updateText() {
        //abychom získali parametry z postu, musíme si zavolat singleton Frontcontrolleru
        $fc                     = Zend_Controller_Front::getInstance();
        //zjistíme si dvoupísmenné jazyky nastavené v configu
        $config                 = Zend_Registry::get('config');        
        $langArray['enabled']   = explode(" ", $config->settings->languages->enabled);
        $text_id                = $fc->getRequest()->getParam('text_id');
        $data                   = array();
        foreach ($langArray['enabled'] as $lang) {
            $title = $fc->getRequest()->getParam('title_'.$lang);
            if (isset($title)) {
                $data['title_'.$lang]  = $title;
            }   
            $text = $fc->getRequest()->getParam('text_'.$lang);
            if ($text) {
                $data['text_'.$lang]   = $text;
            }
        }
        $where = $this->getAdapter()->quoteInto('text_id = ?', $text_id);
        try {
            $this->update($data, $where);
        } catch (Zend_Exception $e) {
            echo "<p>Caught exception in Orders->fetchOrders(): " . get_class($e) . "</p>";
            echo "<p>Message: " . $e->getMessage() . "</p>";
        }
    }
    
    public function updateBackup($backuptable) {
        //abychom získali parametry z postu, musíme si zavolat singleton Frontcontrolleru
        $fc                     = Zend_Controller_Front::getInstance();
        //zjistíme si dvoupísmenné jazyky nastavené v configu
        $config                 = Zend_Registry::get('config');        
        $langArray['enabled']   = explode(" ", $config->settings->languages->enabled);
        $data                   = array();
        foreach ($langArray['enabled'] as $lang) {
            $title = $fc->getRequest()->getParam('title_'.$lang);
            if (isset($title)) {
                $data['title_'.$lang]  = $title;
            }   
            $text = $fc->getRequest()->getParam('text_'.$lang);
            if ($text) {
                $data['text_'.$lang]   = $text;
            }
        }
        $where = $this->getAdapter()->quoteInto('text_id = ?', $backuptable);
        try {
            $this->update($data, $where);
        } catch (Zend_Exception $e) {
            echo "<p>Caught exception in Orders->fetchOrders(): " . get_class($e) . "</p>";
            echo "<p>Message: " . $e->getMessage() . "</p>";
        }
    }
    
    function getEmailText($transport = null, $payment = null) {        
        $session = new Zend_Session_Namespace('Default');
        $lang = $session->lang;
        $locale = $session->locale;
        // transport
        // 3 = Česká pošta 
        // 6 = Česká pošta express tarif
        // 5 = Česká pošta economy tarif
        if (in_array($transport, array('3', '5', '6'))) {
            // dobírka
            if ($payment == '2') {
                $variant = '1';
            // převod na kč, slovenský a SIPO účet
            } else if (in_array($payment, array('7', '9', '10'))) {
                $variant = '7';
            // paypal/karta
            } else {
                $variant = '2';
            }
        // 1 = Osobní odběr
        } else if ($transport == '1') {
            // převod na kč, slovenský a SIPO účet
            if (in_array($payment, array('7', '9', '10'))) {
                $variant = '9';
            // platba při osobním převzetí
            } else if ($payment == '3') {
                $variant = '5';
            // dobírka
            } else {
                $variant = '6';
            }
        // 2 = PPL/DHL/balík
        // 4 = Pošta bez hranic
        // 7 = PPL na slovensko    
        } else if ($transport == '4') {
            // převod na kč, slovenský a SIPO účet
            if (in_array($payment, array('7', '9', '10'))) {
                $variant = '12';
            // paypal/karta
            } else if ($payment == '1') {
                $variant = '11';
            } else {
                $variant = '10';
            }
        
        } else {
            // převod na kč, slovenský a SIPO účet
            if (in_array($payment, array('7', '9', '10'))) {
                $variant = '8';
            // paypal/karta
            } else if ($payment == '1') {
                $variant = '3';
            } else {
                $variant = '4';
            }
        }
        if (empty($transport) && empty($payment)) {
            $variant = 'sent';
        }
        $db = Zend_Registry::get('db');
        $sql = "SELECT title_$lang AS title, text_$lang AS text, text_en AS text "
                . "FROM $this->_name "
                . "WHERE $this->_primary = 'email$variant' AND locale = '$locale'";
        try {
            $result = $db->fetchRow();
        } catch (Zend_Exception $e) {
            echo "<p>Caught exception in Orders->fetchOrders(): " . get_class($e) . "</p>";
            echo "<p>Message: " . $e->getMessage() . "</p>";
            echo "<p>SQL: $sql</p>";
        }
        return $result;
    }
    
    function array_push_assoc($array, $key, $value){
        $array[$key] = $value;
        return $array;
    }
}