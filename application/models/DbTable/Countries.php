<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class Model_DbTable_Countries extends Zend_Db_Table_Abstract {

    protected $_name = 'eshop_countries';
    protected $_primary = 'country_id';
    
    public function fetchCountries() {
        $defaultSession = new Zend_Session_Namespace('Default');
        $query = $this->select()->from($this->_name, array('country_id', 'name_'.$defaultSession->lang.' AS name'));
        $result = $this->fetchAll($query)->toArray();
        return $result;
    }
}