<?php

class Admin_MailController extends Zend_Controller_Action
{

    public function init() {
    }
    
    private function checkRelevance() {
        $hour       = date('H');
        $day        = date('w');
        $send       = false;
        //zkusíme, zda je správná hodina
        if ($hour >= 10 && $hour <= 13) {
            $send = true;
        } else {
            echo "<p>Jídelní lístky lze odesílat pouze dopoledne!</p>";
            $send = false;
        }
        //zkusíme, zda je správný den (Po až Pá)
        if ($day >= 1 && $day <= 5) {
            $send = true;
        } else {
            echo "<p>Jídelní lístky lze odesílat pouze v pracovní den!</p>";
            $send = false;
        }
        return $send;
    }
    
    public function indexAction() {
        $token      = $this->getParam('token');
        $today      = date('Y-m-d');
        
        //zjistíme, zda je vůbec co odeslat, pokud není, odešle se upozornění na uvedené adresy, že někdo zapoměl vyplnit lístek
        $mail       = new Zend_Mail('utf-8');
        $menumodel  = new Model_DbTable_Menus();
        $menu       = $menumodel->fetchRow("menu_id = '$today'");
        
        //pokud je vyplněná nabídka a souhlasí token od kronu, můžeme posílat maily
        if (!empty($menu->menu_cz) && $token == 'sdfvsdhgnbfghjnmghjknh') {             
            echo "<p>Token souhlasí!</p>";
            $send = true;  
        //pokud souhlasí token, tak je funkce spouštěna z cronu a měl by se odeslat varovný email    
        } else if (empty($menu->menu_cz) && $token == 'sdfvsdhgnbfghjnmghjknh') {           
            echo "<p>Token souhlasí, ale není vyplněna nabídka!</p>";      
            try {
                $mail->setSubject("Polední nabídka ke dni ".date('d. n. Y')." NENÍ ROZESLÁNA!");
                $mail->setBodyText('Pozor! Vzhledem k tomu, že není vyplněna polední nabídka na tento den se neodeslal automatický email. Je třeba nejdřív vyplnit nabídku a pak odeslat mail manuálně.');
                $mail->setFrom('info@pivovarskyklub.com', 'Pivovarský klub');  
                $mail->addTo('jan_dockal@seznam.cz');
                $mail->send();
            } catch (Exception $e) {
                $e;
            } 
            $send = false;
        //pokud je nabídka na den prázdná, ale chybí token (někdo se to snaží spouštět manuálně) tak se vypíše chyba!
        } else {        
            echo "<p>Všechno špatně!</p>";
            $send = false;
        }
        
        //pokud jsou splněny předchozí podmínky, jedeme dál
        if ($this->checkRelevance() && $send) {
            $mailmodel  = new Model_DbTable_Mailinglist();            
            $recipients = $mailmodel->fetchAllActive();
            
            //připojíme k emailu "akci", nebo doplňující informaci či co...
            $modelTexts = new Model_DbTable_Texts();
            $texts = $modelTexts->fetchTexts();
            $akce = $texts['akce']['text'];
            
            //$mail->setHeaderEncoding(Zend_Mime::ENCODING_BASE64);
            $mail->setSubject("Polední nabídka ke dni ".date('d. n. Y'));
            $mail->setFrom('info@pivovarskyklub.com', 'Pivovarský klub');     
            
            echo "<table class='admintable'>";
            echo "<th><tr>";
            echo "<td>email</td><td>naposledy odeslán</td><td>status</td>";
            echo "</tr></th>";
            foreach ($recipients as $value) {       
                echo "<tr>";
                echo "<td>".$value->mail."</td>";
                echo "<td>".$value->last_sent."</td>";
                
                if ($today == $value->last_sent) {
                    echo "<td>Email byl dnes již odeslán!</td>";
                } else {
                    try {
                        //pro každého uživatele se generuje link na odhlášení zvlášť
                        $link = "<br/><br/>Pokud již nechcete tyto polední nabídky přijímat, prosím klikněte na odkaz <a href='http://www.pivovarskyklub.com/mailinglist/delete?token=$value->token'>[ODHLÁSIT]</a>";
                        $mail->clearRecipients();
                        $mail->addTo($value->mail);
                        $mail->setBodyHtml($menu->menu_cz.$akce.$link);
                        $mail->send();
                        $mailmodel->emailSent($value->mail_id);
                        echo "<td>Zend: Email odeslán!</td>";
                    } catch (Exception $e) {
                        echo "<td>Zend: Něco selhalo!</td><pre>".$e."</pre>";
                    }
                }
                echo "</tr>";
            } 
            echo "</table>";
        } else {
            try {
                $mail->setSubject("Polední nabídka ke dni ".date('d. n. Y')." NENÍ ROZESLÁNA!");
                $mail->setBodyText('Lístek se neodeslal, nevím proč.');
                $mail->setFrom('info@pivovarskyklub.com', 'Pivovarský klub');  
                $mail->addTo('jan_dockal@seznam.cz');
                $mail->send();
            } catch (Exception $e) {
                $e;
            } 
        }  
    }
    
    public function confirmAction() {
        $order_id = $this->getParam('order_id');         
        $status_id = $this->getParam('status_id'); 
        
        $order = new Model_EshopOrder($order_id);
        $model = new Model_DbTable_EshopTexts();
        if ($order->getDeliveryId() == 1 || $order->getDeliveryId() == 3) {
            $text = $model->find('email_osobne_pripraveno')->toArray();
        } else {
            $text = $model->find('email_ppl_odeslano')->toArray();
        }
        //tohle je ochcávka kvůli tomu aby šlo odeslat email i u ukončených objednávek
        //u ukončených objednávek se totiž vrací NULL jako next status
        $nextstatus = $order->getNextStatus();
        if (empty($nextstatus)) {
            if ($order->getDeliveryId() == 1) {
                $nextstatus = 6;
            } else { 
                $nextstatus = 5;                        
            }
        }
        $data = array(
            'order_id' => $order_id,
            'status_id' => $nextstatus, 
            'email' => $order->getEmail(),
            'subject' => $text[0]['title_cz'],
            'text' => $text[0]['text_cz'].$this->getProductTable($order)
        );
        if ($order->checkConfirmationSent()) {
            echo "<h1>Email o zásilce byl již odeslán a nelze jej odeslat znovu.</h2>";
        } else {            
            $form = new Form_Mailform('/admin/mail/send/');
            $form->populate($data);
            $this->view->form = $form;
        }
        $data['status_id'] = $status_id;
        $skip = new Form_Skipform('/admin/orders/status/');
        $skip->populate($data);
        $this->view->skip = $skip;
    }

    public function sendAction() {
        $order_id = $this->getParam('order_id'); 
        $email = $this->getParam('email');
        $subject = $this->getParam('subject'); 
        $text = $this->getParam('text');         
        $status_id = $this->getParam('status_id');
                
        $mail = new Zend_Mail('utf-8');
        $mail->setFrom('eshop@pivovarskyklub.com', 'Pivovarský klub E-shop');
        $mail->addTo($email);
        $mail->setBodyHtml($text);    
        $mail->setSubject($subject);
        
        try {
            $mail->send();
        } catch (Exception $e) {
            echo "Caught exception: " . get_class($e) . "\n";
            echo "Message: " . $e->getMessage() . "\n";
        }        
        if (!empty($status_id)) {
            $this->_redirect('/admin/orders/status/?order_id='.$order_id.'&status_id=-'.$status_id);
        }       
        //echo '/admin/orders/status/?order_id='.$order_id.'&status_id=-'.$status_id;
    }
    
    private function getProductTable($order) {
        //tabulka s objednanými věcmi
        $productTable = "<br/><table width='560'><tr><td>ID</td><td>produkt</td><td>množství</td><td>cena za kus</td><td>cena celkem</td>";
        $totalPrice = 0;
        foreach ($order->getOrderProducts() as $product) {
            $productTable .= "<tr><td>" . $product->getProductId() . "</td><td>" . $product->getTitle() . "</td><td align='right'>" . $product->getQuantity() . "</td><td align='right'>" . $product->getPrice() . ",-kč</td><td align='right'>" . $product->getTotalPrice() . ",-kč</td></tr>";
            $totalPrice += $product->getTotalPrice();
        }
        $deliveryPrice = $order->getDeliveryPrice();
        if (!empty($deliveryPrice)) {
            $productTable .= "<tr><td></td>"
                            . "<td>Doprava</td><td align='right'>" . $order->getDeliveryCount() . "</td>"
                            . "<td align='right'>" . $order->getDeliveryPrice() . ",-kč</td>"
                            . "<td align='right'>" . $order->getDeliveryTotal() . ",-kč</td></tr>";
            $totalPrice += $order->getDeliveryTotal();
        }
        $paymentPrice = $order->getPaymentPrice();
        if (!empty($paymentPrice)) {
            $productTable  .= "<tr><td></td><td>" . $order->getPaymentTitle() . "</td>"
                            . "<td align='right'>" . $order->getPaymentCount() . "</td>"
                            . "<td align='right'>" . $order->getPaymentPrice() . ",-kč </td>"
                            . "<td align='right'>" . $order->getPaymentTotal() . ",-kč</td></tr>";
            $totalPrice += $order->getPaymentTotal();
        }
        $productTable .= "<tr><td></td><td>Celkem</td><td></td><td></td><td align='right'>" . $totalPrice . ",-kč</td></tr>";
        $productTable .= "</table><br/>";
        $productTable .= "<br/>Všechny ceny jsou uvedeny včetně DPH.";
        
        return $productTable;
    }
}


